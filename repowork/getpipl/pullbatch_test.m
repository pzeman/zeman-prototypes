%
% do a test pull of data from the pipl website
%
% Automatically pull 5 records from the emailArray file and store the data from these records
% in a directory that corresponds to the GravityLabID and a subdirectory that corresponds to the 10000's of the
% GravityLabID. This will limit the number of files in each directory to 10,000.

function [resultString, nDownloaded] = pullbatch_test(emailArray,cityStateLookupArray)

resultString=[];
nDownloaded = 0;

DOREALQUERYFLAG = 0;

emailidx = 1;
firstnameidx = 2;
lastnameidx = 3;
citycodeidx = 4;
statecodeidx = 5;
countrycodeidx = 23;

Nentries = size(emailArray,1);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create cityStateLookupArray
%cityStateLookupArray = readmixedcsvall('us_cities_and_states.csv','|');






  %checkCityStateMatchBool('Seattle','WA',cityStateLookupArray)

%printf('end test\r\n');
%pause



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% STEP THROUGH EACH ENTRY IN emailArray and get additional data from pipl

for i = 1:Nentries

   emailAddressString = char(emailArray{i,emailidx});
   firstNameString = char(emailArray{i,firstnameidx});
   lastNameString = char(emailArray{i,lastnameidx});
   cityCodeString = char(emailArray{i,citycodeidx});
   stateCodeString = char(emailArray{i,statecodeidx});
  % countryCodeString = char(emailArray{i,countrycodeidx});
   countryCodeString = []; %do not use country
  


   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %% CHECK IF LOCATION INFORMATION IS VALID OR NONSENSE. IF IT IS NONSENSE, DO NOT USE IT IN THE QUERY
   % Check if correct city and state combination.

   if ~isempty(cityCodeString) && ~isempty(stateCodeString) %if city is provided, check to see that it matches the state full name or code
     
       if checkCityStateMatchBool(cityCodeString,stateCodeString,cityStateLookupArray)
       
         goodCityAndState_FLAG = 1;
        % cityCodeString
        % stateCodeString

       else
         goodCityAndState_FLAG = 0;
       
       end
   else
       %since we dont have both city and state to verify validity, do not use location information in the query
       goodCityAndState_FLAG = 0;  
     
   end

   
   
   
   
   



 %Only do query if the state, country, city make sense and are possible.
   if goodCityAndState_FLAG == 1
    [resultString{i}] = getquery(emailAddressString,firstNameString,lastNameString,cityCodeString,stateCodeString,countryCodeString,DOREALQUERYFLAG);
    nDownloaded = nDownloaded +1;
   elseif goodCityAndState_FLAG == 0
     %do the query without location information
    % stateCodeString =[];
    % cityCodeString =[];
    % [resultString{i}] = getquery(emailAddressString,firstNameString,lastNameString,cityCodeString,stateCodeString,countryCodeString,DOREALQUERYFLAG);  
    %nDownloaded = nDownloaded +1;  
   end
   

   
   
end %for look for all email entries









%%%%%%%%%%%%%%%%%%%%%%%%%%% AUX FUNCTIONS


function [resultBool] = checkCityStateMatchBool(cityCodeString,stateCodeString,cityStateLookupArray)
%
% Return 1 (true) if the city is located in the paired state
% Return 0 (false) if the city is not in the paired state
%
resultBool = 0; %default return state is FALSE
noStateAbbreviation_FLAG=0;
noState_FLAG = 0;

%first find all indices where the target state is listed
I = strmatch(    upper(stateCodeString)   ,   upper(char(cityStateLookupArray{:,1}))  ,'exact'); %look at the abbreviation





if ~isempty(I)

  Icity = strmatch(    upper(cityCodeString)   ,   upper(char(cityStateLookupArray{I,3}))  );
  
  %Icity
  
  if ~isempty(Icity)
%    fprintf('city not found for the state specified\r\n');
  %  return
    resultBool = 1;
    return
  end
end

%first find all indices where the target state is listed
I = strmatch(    upper(stateCodeString)   ,   upper(char(cityStateLookupArray{:,2}))  ,'exact'); %look at the full state name


if ~isempty(I)

  Icity = strmatch(    upper(cityCodeString)   ,   upper(char(cityStateLookupArray{I,3}))  );
  if ~isempty(Icity)
     resultBool =1 ;    
     return    
  end
end




%find the city name listed by the subset indices I


%FOUND THE CITY IN THE STATE SPECIFIED













function [resultString] = getquery(emailAddressString,firstNameString,lastNameString,cityCodeString,stateCodeString,countryCodeString,DOREALQUERYFLAG)
% Return a string containg the text to be parsed containing the data
% Return emtpy string if an error getting the data
%
%
% The pipl interfaces returns information from multiple sources and is not email dependant.  Because data come
% from multiple sources, the query
% results improve if a first and last name and State of residence are included in the query.
% Since the results returned are not dependant on the email address supplied, it IS POSSIBLE
% THAT THE RESULTS RETURNED ARE NOT CONNECTED TO THE EMAIL ADDRESS SUPPLIED
%
% USE ISO country codes
%

baseQueryString = 'https://api.pipl.com/search/v4/?';
keyString = 'key=d62man0xm85d67lilgggvz90';

if ~isempty(stateCodeString)
stateCodeString = ['state=' stateCodeString '&'];
end


if ~isempty(emailAddressString)
% Prepare email address string to be used in the query. The query requires %40 to replace the @
%emailAddressString = 'test@test.com'
emailAddressString = strrep(emailAddressString,'@','%40');
emailAddressString = ['email=' emailAddressString '&'];
end

if ~isempty(firstNameString)
firstNameString = ['first_name=' firstNameString '&'];
end

if ~isempty(lastNameString)
%cityCodeString = strrep(cityCodeString,'@','+');
lastNameString = ['last_name=' lastNameString '&'];
end


if ~isempty(cityCodeString)
cityCodeString = strrep(cityCodeString,' ','+');
cityCodeString = ['city=' cityCodeString '&'];
end


if ~isempty(countryCodeString)
countryCodeString = ['country=' countryCodeString '&'];
end


inputstr = [baseQueryString emailAddressString firstNameString lastNameString countryCodeString stateCodeString cityCodeString keyString];


%fprintf('*%s*\r\n',inputstr);



resultString=[];
%inputstr = sprintf('https://api.pipl.com/search/v4/?email=craignjulieb%40yahoo.com&state=PA&key=d62man0xm85d67lilgggvz90');
%inputstr = 'https://api.pipl.com/search/v4/?email=craignjulieb%40yahoo.com&first_name=Craig&last_name=Butch&country=usa&state=PA&key=d62man0xm85d67lilgggvz90';
if DOREALQUERYFLAG == 1
  resultString = urlread(inputstr);

else
  resultString = 'dummystring';
end


%https://api.pipl.com/search/v4/?email=pzeman%40hotmail.com&first_name=Philip&last_name=Zeman&country=US&state=PA&key=d62man0xm85d67lilgggvz90
%https://api.pipl.com/search/v4/?email=pzeman%40hotmail.com&first_name=Philip&last_name=Zeman&country=US&state=PA&key=d62man0xm85d67lilgggvz90

