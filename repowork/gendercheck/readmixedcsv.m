%
%
% Read list of names from file. 
% Convert to upper case.
% Search for database string in the email address match to M or F
%



function lineArray = readlist(fileName,delimiter,maxlines)


lineArray=[];




fid = fopen(fileName,'r');   %# Open the file
fprintf('Counting how many lines in text file\r\n');
n = 0;
tline = fgetl(fid);
while ischar(tline)
  tline = fgetl(fid);
  n = n+1;
end
fclose(fid);

nlines = n;
fprintf('There are %i lines in this file\r\n',nlines);
fflush(stdout);


%%%% READ THE FILE

fid = fopen(fileName,'r');   %# Open the file
fprintf('Reading lines\r\n');
fflush(stdout);

lineArray = cell(100,1);     %# Preallocate a cell array (ideally slightly
                               %#   larger than is needed)
lineIndex = 1;               %# Index of cell to place the next line in
nextLine = fgetl(fid);       %# Read the first line from the file
while (~isequal(nextLine,-1)) && (lineIndex < maxlines)         %# Loop while not at the end of the file
    lineArray{lineIndex} = nextLine;  %# Add the line to the cell array

  %size(lineArray)
%lineArray{lineIndex}

%lineIndex
%pause

    lineIndex = lineIndex+1;          %# Increment the line index
    nextLine = fgetl(fid);            %# Read the next line from the file
end
fclose(fid);                 %# Close the file



fprintf('Parsing data\r\n');
fflush(stdout);



lineArray = lineArray(1:lineIndex-1);  %# Remove empty cells, if needed
for iLine = 1:lineIndex-1              %# Loop over lines
    lineData = textscan(lineArray{iLine},'%s',...  %# Read strings
                        'Delimiter',delimiter);
    lineData = lineData{1};              %# Remove cell encapsulation
    if strcmp(lineArray{iLine}(end),delimiter)  %# Account for when the line
      lineData{end+1} = '';                     %#   ends with a delimiter
    end
    lineArray(iLine,1:numel(lineData)) = lineData;  %# Overwrite line data
end


%end %EOD OF OCTAVE FUNCTION





