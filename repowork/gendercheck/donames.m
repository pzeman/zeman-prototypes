%
%
% Do gender validation based on names in email addresses
%
% Output a certainty statistic
%
%
%
% IMPROVEMENTS TO DO:
%
% Filter feminine and masculine list by frequency of occurance of each name for each gender. Remove those names that are 
% infrequencyly used for that gender. For example, a few people in the USA census data have decided to call their girls "LARRY". We do not
% want Larry to be included as a feminine name.
% 
% Should identify names such as "bo" that match many email addresses that probably are not beloning to someone named "Bo". There
% are some very short USA names such as "Bo" that will capture inappropriate email addresses.
% We can: (1) ignore 2 character first names, or (2) filter names that are statistically unusual in the appearance in emails addresses
%
% Emails with both sexes: dennisandlannawalker@hotmail.com, (man and wife?)
% Emails with both sexes: billieonair@yahoo.ca (bill and billie
%
% Remove names from masculine list that have been feminised (such as adding a 'a' to the end of a masculine name)
% 
% email regular expressions: email = '[a-z_]+@[a-z]+\.(com|net)';
%
%

%emailcsvfilepathname = '/media/NAS_clinezeman/clinezeman/CLIENT_gravitylab/GLDatabase/yahoo_info.txt';
emailcsvfilepathname = '/home/abv/CLIENT_gravitylab/GLDatabase/yahoo_info.txt';


%%%%% FLAGS FOR CHOOSING DATA SOURCES

LOADFROMMATFLAG_emailArray=1; %0=load raw, 1 = load .mat


%THE ROW LOCATION OF EACH TYPE OF DATA
emailaddressidx= 1;
sexidx = 10;
GravityLabID_index=33;
operatingsystemidx = 16;
LOADALLDATAFLAG = 1; %load all records (takes lots of time)


operatingSystemCategories = {'CHROME OS','WINDOWS 7','WINDOWS','WINDOWS 8','WINDOWS XP','WINDOWS NT 4.0','WINDOWS RT','MAC OS X','ANDROID','WINDOWS VISTA','IOS','BLACKBERRY OS','OTHER'};




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% READ AND CURATE MASCULINE AND FEMININE NAME DICTIONARIES

%Read dictionary of Male Names
lineArray = readmixedcsvall('census-dist-male-first.txt',' ');
Nmasculinenames = size(lineArray,1);
masculinenames = cell(Nmasculinenames,1);
%copy for memoryt management
for i = 1:Nmasculinenames
  masculinenames{i} = lineArray{i,1};
end



%Read dictionary of Female Names
lineArray = readmixedcsvall('census-dist-female-first.txt',' ');
Nfemininenames = size(lineArray,1);
femininenames = cell(Nfemininenames,1);
%copy for memoryt management
for i = 1:Nfemininenames
  femininenames{i} = lineArray{i,1};
end


%%NOTE THAT JAMES IS IN BOTH THE FEMININE AND MASCULINE NAMES LIST.
% Doing a separate evalaution of FEM vs MAS names for email address and then separate for
% the first name field results in a disjonit confidence estimate.  It should be done doing the following:%
%CASE 1: the name in the "first name" field is found somewhere in the email address
% if the name is in both places, the name confidence  = 2
%
%CASE 2: the name is found in the "first name" field but not in the email address
% confidence = 1
%
% CASE 3: the name is found in the email address and the first name field is emptyCells
% confidence = 0.5 (inferred)
% 
% GENDER ASSIGNMENT: 
%
% CASE 1: we have gender and it is explicit in the data = confidence 1
%
% CASE 2: we use the first name to estimate the gender based on probablity confidence = 0.5
% if the probabily is higher that the name is male than female, then assign mail, and vice versa.
% ** USE a confidence and an INFERRED classification.


%READ THE FREQUENCY OF OCCURANCE 
feminineFreq = []; 
data = textread('census-dist-female-first.txt','%s','delimiter','\n','whitespace','');
for lineNum = 1: size(data,1)
numtemp = regexp(data(lineNum),['\d.+'],'match'); %get digits with reg expression
numtemp = str2num(char(numtemp));
feminineFreq(lineNum) = numtemp(1);
end

masculineFreq = [];
data = textread('census-dist-male-first.txt','%s','delimiter','\n','whitespace','');
for lineNum = 1: size(data,1)
numtemp = regexp(data(lineNum),['\d.+'],'match'); %get digits with reg expression
numtemp = str2num(char(numtemp));
masculineFreq(lineNum) = numtemp(1);
end

Ifeminine = find(feminineFreq <= 0.001);
Imasculine = find(masculineFreq <= 0.004);

%REMOVE INFREQUENT NAMES FROM LISTS
femininenames(Ifeminine) = [];
Nfemininenames  = size(femininenames,1);
masculinenames(Imasculine) =[];
Nmasculinenames = size(masculinenames,1);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%% READ EMAIL DATA FILE


if LOADFROMMATFLAG_emailArray == 1

   load(yahoo_info_emailArray.mat);

else %LOAD FROM RAW and CREATE MAT
   

   %Get list of emails 
   if LOADALLDATAFLAG == 0
     emailArray = readmixedcsv(emailcsvfilepathname,',',5000);
   else
     emailArray = readmixedcsvall(emailcsvfilepathname,',');
   end
   %get the email addresses, SKIP FIRST LINE
   
   
   emailArrayHeadings=[];
   for k =1:size(emailArray,2)
   emailArrayHeadings{k} = emailArray{1,k};
   end
   
   emailArrayHeadings{GravityLabID_index} = 'GravityLab_index';
   emailArray(1,:) = []; %remove heading for the email addresses
   Nemails = size(emailArray,1);
   
   % ASSIGN and Append a GravityLabID to each email address in emailArray
   % Start from 1 and go to ....
   GravityLabID=[];
   
   for i = 1:Nemails
      emailArray{i,33} = i;
   end

   
   %STORE THE LOADED DATA IN BINARY FORMAT FOR FAST READING
   databasefilepathname = '/home/abv/CLIENT_gravitylab/GLDatabase/yahoo_info_17NOV2015.mat';
   save(databasefilepathname,'emailArray','emailArrayHeadings','-V7');
   
   
   
end

























%%%%%%%%%%%%%%%%%%%%%%%%
%%% GET INDICES OF EMAIL ADDRESSES WITH MASCULINE STRINGS IN THE EMAIL
% Estimate Gender from the email addreses by finding first names in the email addresses

masculineemailsidx = zeros(1,Nemails);

for masculinename_cnt = 1:Nmasculinenames
  I = strmatch(    upper(char(masculinenames{masculinename_cnt}))   ,   upper(char(emailArray{:,1}))  );
 %I contains the indices in emailArray that match the name
  masculineemailsidx(I) = 1; %flag with 1 those that are masculine
end





%%%%%%%%%%%%%%%%%%%%%%%%
%%% GET email indices with feminine names


feminineemailsidx = zeros(1,Nemails);
%Estimate Gender from the email addreses by finding first names in the email addresses
for femininename_cnt = 1:Nfemininenames
  I = strmatch(    upper(char(femininenames{femininename_cnt}))   ,   upper(char(emailArray{:,1}))  );
  feminineemailsidx(I) = 1; %flag with 1 those that are masculine
end







%%%% ADDED 04NOV2015 to read in the first name fields already in the data
%%%%%%%%%%%%%%%%%%%%%
%%% Get which indices have a female name in the data and compare to standard female names list
Istruct = zeros(1,Nemails);
%Estimate Gender from the email addreses by finding first names in the email addresses
for femininename_cnt = 1:Nfemininenames
  I = strmatch(    upper(char(femininenames{femininename_cnt}))   ,   upper(char(emailArray{:,2}))  );
 %I contains the indices in emailArray that match the name
  Istruct(I) = 1; %flag with 1 those that are masculine
%strfind( malenames, char(emailArray(:,1)) );
end
femininenamesidx = Istruct;



%%% ADDED 04NOV2015 to read in the first name fields already in thedata
%%%%%%%%%%%%%%%%%%%%%
%%% Get which indices have a female name in the data and compare to standard female names list
Istruct = zeros(1,Nemails);
%Estimate Gender from the email addreses by finding first names in the email addresses
for masculinename_cnt = 1:Nmasculinenames
  I = strmatch(    upper(char(masculinenames{masculinename_cnt}))   ,   upper(char(emailArray{:,2}))  );
 %I contains the indices in emailArray that match the name
  Istruct(I) = 1; %flag with 1 those that are masculine
%strfind( malenames, char(emailArray(:,1)) );
end
masculinenamesidx = Istruct;







%%%%%%%%%%%%%%%%%%%%%%%%%
%%% CHECK WHICH EMAIL ADDRESSES IN BOTH GENDER NAME LISTS


joinedlist = [feminineemailsidx; masculineemailsidx];
joinedlist = sum(joinedlist,1);
I = find(joinedlist == 2); %find those entries that are in both gender lists
androgynyemailsidx = zeros(1,Nemails);
androgynyemailsidx(I) = 1;



%%%%%%%%%%%%%%%%%%%%%%%%%
%%% CHECK WITCH EMAILS ARE IN NEITHER MASCULINE OR FEMININE

joinedlist = [feminineemailsidx; masculineemailsidx];
joinedlist = sum(joinedlist,1);
I = find(joinedlist == 0);
noinfoemailsidx = zeros(1,Nemails);
noinfoemailsidx(I) = 1;











%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% MARK a 1 or 0 in the list if the gender is male

IstructM = zeros(1,Nemails);
%Estimate Gender from the email addreses by finding first names in the email addresses
%for femininename_cnt = 1:Nfemininenames
  I = strmatch(    'M'   ,   upper(char(emailArray{:,sexidx})) ,'exact' );
 %I contains the indices in emailArray that match the name
  IstructM(I) = 1; %flag with 1 those that are masculin
%strfind( malenames, char(emailArray(:,1)) );
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% MARK a 1 or 0 in the list if the gender is female
IstructF = zeros(1,Nemails);
%Estimate Gender from the email addreses by finding first names in the email addresses
%for femininename_cnt = 1:Nfemininenames
  I = strmatch(    'F'   ,   upper(char(emailArray{:,sexidx})) ,'exact' );
 %I contains the indices in emailArray that match the name
  IstructF(I) = 1; %flag with 1 those that are masculin
%strfind( malename



%get the gender from the gender field, SKIP FIRST LINE
%lineArray{:,10}

%%%%%%%%%%%%%%%%%%%%%%%%
%%% CALCULATION CONFIDENCE MASKS


CI_masculineidx = (masculinenamesidx + masculineemailsidx); %0 if not masculine, 1 if one field indicates masculine, 2 if both fields indicate mascuilne
CI_feminineidx = (femininenamesidx + feminineemailsidx);






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  GENERATE NEW LISTS USING MASKS AND DATA


%Ichoose = IstructF | feminineNamesMoreFreqThanThreshIdx;
%
%Commented out and replaced with CI below
%Ifeminine = find(feminineemailsidx == 1);   %possibly feminine
%Imasculine = find(masculineemailsidx == 1); %possibly masculine

Ifeminine = find(CI_feminineidx >= 2);   %possibly feminine
Imasculine = find(CI_masculineidx >= 2); %possibly masculine

Ioverlap_masculinefeminine = find(sum([feminineemailsidx; masculineemailsidx],1) == 2); %the masculine feminine overlap

Ncolumns = size(emailArray,2);
%emailArray_feminine_noinfo = emailArray([Ifeminine Inoinfoemails],:);
%emailArray_feminine_noinfo = emailArray_feminine_noinfo';
emailArray_feminine = emailArray([Ifeminine],:);
emailArray_feminine = emailArray_feminine';

%emailArray_masculine_noinfo = emailArray([Imasculine Inoinfoemails],:);
%emailArray_masculine_noinfo = emailArray_masculine_noinfo';
emailArray_masculine = emailArray([Imasculine],:);
emailArray_masculine = emailArray_masculine';



%%%HERE


%turn warnings about string conversions off
warning off

fid = fopen('yahoo_info_masculine_lowconfidence_06NOV2015.csv','w')
fprintf(fid,'email,first_name,last_name,city,state,ip,from_location,gender,address,zip1,zip2,phone1,phone2,dob,category,platform,browser_type,browser_version,latitude,longitude,mobile,mobile_type,country,region,region_name,postal_code\n');
  fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',emailArray_masculine{1:end,:});
fclose(fid);

fprintf(fid,'email,first_name,last_name,city,state,ip,from_location,gender,address,zip1,zip2,phone1,phone2,dob,category,platform,browser_type,browser_version,latitude,longitude,mobile,mobile_type,country,region,region_name,postal_code\n');
fid = fopen('yahoo_info_feminine_lowconfidence_06NOV2015.csv','w')
  fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',emailArray_feminine{1:end,:});
fclose(fid);









%SPLIT THE MASCUILINE AND FEMININE INTO OS GROUPS



%oslist ={'WINDOWS','IOS','MAC'}

  windowsemailsidx = zeros(1,Nemails);
  I = strmatch(    'WINDOWS'   ,   upper(char(emailArray{:,operatingsystemidx}))  );
  windowsemailsidx(I) = 1; %flag with 1 those that are masculine




  iosemailsidx = zeros(1,Nemails);
  I = strmatch(    'IOS'   ,   upper(char(emailArray{:,operatingsystemidx}))  );
  iosemailsidx(I) = 1; %flag with 1 those that are masculine


  macemailsidx = zeros(1,Nemails);
  I = strmatch(    'MAC'   ,   upper(char(emailArray{:,operatingsystemidx}))  );
  macemailsidx(I) = 1; %flag with 1 those that are masculine

  allmacemailsidx = (macemailsidx | iosemailsidx);
 



 androidemailsidx = zeros(1,Nemails);
  I = strmatch(    'ANDROID'   ,   upper(char(emailArray{:,operatingsystemidx}))  );
  androidemailsidx(I) = 1; %flag with 1 those that are masculine


 blackberryemailsidx = zeros(1,Nemails);
  I = strmatch(    'BLACKBERRY'   ,   upper(char(emailArray{:,operatingsystemidx}))  );
  blackberryemailsidx(I) = 1; %flag with 1 those that are masculine





%%%%% SAVE FILES WITH OS IDENTIFIED

 %Choose gender confidence
 Ifeminine = find(CI_feminineidx >= 1);   %possibly feminine
 Imasculine = find(CI_masculineidx >= 1); %possibly masculine

 %select feminine and mac
 Imaskfeminine = zeros(1,Nemails);
 Imaskfeminine(Ifeminine) = Ifeminine;
Imask_feminine_allmac= (Imaskfeminine & allmacemailsidx);


 %select masculine and allmac
 Imaskmasculine = zeros(1,Nemails);
 Imaskmasculine(Imasculine) = Imasculine;
 Imask_masculine_allmac= (Imaskmasculine & allmacemailsidx);


 %select feminine and mac
 Imaskfeminine = zeros(1,Nemails);
 Imaskfeminine(Ifeminine) = Ifeminine;
Imask_feminine_ios= (Imaskfeminine & iosemailsidx);


 %select masculine and allmac
 Imaskmasculine = zeros(1,Nemails);
 Imaskmasculine(Imasculine) = Imasculine;
 Imask_masculine_ios= (Imaskmasculine & iosemailsidx);



 %select feminine and mac
 Imaskfeminine = zeros(1,Nemails);
 Imaskfeminine(Ifeminine) = Ifeminine;
Imask_feminine_mac= (Imaskfeminine & macemailsidx);

 %select masculine and allmac
 Imaskmasculine = zeros(1,Nemails);
 Imaskmasculine(Imasculine) = Imasculine;
 Imask_masculine_mac= (Imaskmasculine & macemailsidx);






%select masculine and wiundows
 Imaskmasculine = zeros(1,Nemails);
 Imaskmasculine(Imasculine) = Imasculine;
 Imask_masculine_windows= (Imaskmasculine & windowsemailsidx);

 %select feminine and windows
 Imaskfeminine = zeros(1,Nemails);
 Imaskfeminine(Ifeminine) = Ifeminine;
Imask_feminine_windows= (Imaskfeminine & windowsemailsidx);



 %select feminine and android
 Imaskfeminine = zeros(1,Nemails);
 Imaskfeminine(Ifeminine) = Ifeminine;
Imask_feminine_android= (Imaskfeminine & androidemailsidx);



%select masculine and android
 Imaskmasculine = zeros(1,Nemails);
 Imaskmasculine(Imasculine) = Imasculine;
 Imask_masculine_android= (Imaskmasculine & androidemailsidx);








 %select feminine and blackberry
 Imaskfeminine = zeros(1,Nemails);
 Imaskfeminine(Ifeminine) = Ifeminine;
Imask_feminine_blackberry= (Imaskfeminine & blackberryemailsidx);



%select masculine and blackberry
 Imaskmasculine = zeros(1,Nemails);
 Imaskmasculine(Imasculine) = Imasculine;
 Imask_masculine_blackberry= (Imaskmasculine & blackberryemailsidx);





%%%% Plot the numbers in each group


Nmasculine_ios = length(find(Imask_masculine_ios));
Nfeminine_ios = length(find(Imask_feminine_ios));


Nmasculine_mac = length(find(Imask_masculine_mac));
Nfeminine_mac = length(find(Imask_feminine_mac));






Nmasculine_windows = length(find(Imask_masculine_windows));
Nfeminine_windows = length(find(Imask_feminine_windows));




Nmasculine_android = length(find(Imask_masculine_android));
Nfeminine_android = length(find(Imask_feminine_android));


Nmasculine_blackberry = length(find(Imask_masculine_blackberry));
Nfeminine_blackberry = length(find(Imask_feminine_blackberry));




tempmasculine = [Nmasculine_allmac Nmasculine_mac Nmasculine_ios Nmasculine_windows Nmasculine_android Nmasculine_blackberry];
tempfeminine = [Nfeminine_allmac Nfeminine_mac Nfeminine_ios Nfeminine_windows Nfeminine_android Nfeminine_blackberry];
%figure; bar[tempfeminine];
figure; bar([tempmasculine; tempfeminine]');
title('fem=r, mas=b; allmac=1, mac=2, ios=3 allwindows=4, allandroid=5, allblackberry=6');





emailArray_feminine_windows = emailArray( find(Imask_feminine_windows)   ,:);
emailArray_feminine_windows = emailArray_feminine_windows';

emailArray_masculine_windows = emailArray( find(Imask_masculine_windows)  ,:);
emailArray_masculine_windows = emailArray_masculine_windows';

warning off

fid = fopen('yahoo_info_masculine_windows_lowconfidence_12NOV2015.csv','w')
fprintf(fid,'email,first_name,last_name,city,state,ip,from_location,gender,address,zip1,zip2,phone1,phone2,dob,category,platform,browser_type,browser_version,latitude,longitude,mobile,mobile_type,country,region,region_name,postal_code\n');
  fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',emailArray_masculine_windows{1:end,:});
fclose(fid);

fprintf(fid,'email,first_name,last_name,city,state,ip,from_location,gender,address,zip1,zip2,phone1,phone2,dob,category,platform,browser_type,browser_version,latitude,longitude,mobile,mobile_type,country,region,region_name,postal_code\n');
fid = fopen('yahoo_info_feminine_windows_lowconfidence_12NOV2015.csv','w')
  fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',emailArray_feminine_windows{1:end,:});
fclose(fid);






emailArray_feminine_ios = emailArray( find(Imask_feminine_ios)   ,:);
emailArray_feminine_ios = emailArray_feminine_ios';

emailArray_masculine_ios = emailArray( find(Imask_masculine_ios)  ,:);
emailArray_masculine_ios = emailArray_masculine_ios';

warning off

fid = fopen('yahoo_info_masculine_ios_lowconfidence_12NOV2015.csv','w')
fprintf(fid,'email,first_name,last_name,city,state,ip,from_location,gender,address,zip1,zip2,phone1,phone2,dob,category,platform,browser_type,browser_version,latitude,longitude,mobile,mobile_type,country,region,region_name,postal_code\n');
  fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',emailArray_masculine_ios{1:end,:});
fclose(fid);

fprintf(fid,'email,first_name,last_name,city,state,ip,from_location,gender,address,zip1,zip2,phone1,phone2,dob,category,platform,browser_type,browser_version,latitude,longitude,mobile,mobile_type,country,region,region_name,postal_code\n');
fid = fopen('yahoo_info_feminine_ios_lowconfidence_12NOV2015.csv','w')
  fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',emailArray_feminine_ios{1:end,:});
fclose(fid);












emailArray_feminine_android = emailArray( find(Imask_feminine_android)   ,:);
emailArray_feminine_android = emailArray_feminine_android';

emailArray_masculine_android = emailArray( find(Imask_masculine_android)  ,:);
emailArray_masculine_android = emailArray_masculine_android';

warning off

fid = fopen('yahoo_info_masculine_android_lowconfidence_12NOV2015.csv','w')
fprintf(fid,'email,first_name,last_name,city,state,ip,from_location,gender,address,zip1,zip2,phone1,phone2,dob,category,platform,browser_type,browser_version,latitude,longitude,mobile,mobile_type,country,region,region_name,postal_code\n');
  fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',emailArray_masculine_android{1:end,:});
fclose(fid);

fprintf(fid,'email,first_name,last_name,city,state,ip,from_location,gender,address,zip1,zip2,phone1,phone2,dob,category,platform,browser_type,browser_version,latitude,longitude,mobile,mobile_type,country,region,region_name,postal_code\n');
fid = fopen('yahoo_info_feminine_android_lowconfidence_12NOV2015.csv','w')
  fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n',emailArray_feminine_android{1:end,:});
fclose(fid);


























% Get the number of completed fields for each column to tell us what data we have most of.
%for i = 1:size(emailArray,1)
Ncells = size(emailArray,2)
Nemails = size(emailArray,1);
nEntries = [];
for i = 1: Ncells 
  emptyCells = cellfun(@isempty,  emailArray(:,i)  );
  nEntries(i) = Nemails -sum(emptyCells);

end

%rank the categories in decreasing order of completion
[Y I] = sort(nEntries,'descend');

categorynames = {'email','first_name','last_name','city','state','ip','from_location','gender','address','zip1','zip2','phone1','phone2','dob','category','platform','browser_type','browser_version','latitude','longitude','mobile','mobile_type','country','region','region_name','postal_code'};
for i = 1:length(categorynames)
fprintf('Category Index(%i) Category(%s) : Number of Entries(%i) \r\n',i,char(categorynames(I(i))),  Y(i)  );
end

figure;

bar(Y);
title('Number of entries in each category (sorted)');
xlabel('Sorted Category Index');
ylabel('Number of Entries In Category');













