
This readme file describes the data for:

% What it does:
%
% Reads raw email csv file
% Reads USA census file for male first names (with frequency of occurrance)
% Reads USA census file for femail first names (with frequency of occurrance)
% Removes low frequency of occurrance names from the female and male lists
% 
% Creates list of emails with a male name in it (1)
% Creates list of emails with a female name in it (2)
%
% Creates list of emails that do not have a male or female name in it (3)
%
% Appends 1 and 3 to create: yahoo_list_masculineandnoinfo.csv
% Appends 2 and 3 to create: yahoo_list_feminineandnoinfo.csv
%
%
%
% IMPROVEMENTS TO DO:
%
% Filter feminine and masculine list by frequency of occurance of each name for each gender. Remove those names that are 
% infrequencyly used for that gender. For example, a few people in the USA census data have decided to call their girls "LARRY". We do not
% want Larry to be included as a feminine name.
% 
% Should identify names such as "bo" that match many email addresses that probably are not beloning to someone named "Bo". There
% are some very short USA names such as "Bo" that will capture inappropriate email addresses.
% We can: (1) ignore 2 character first names, or (2) filter names that are statistically unusual in the appearance in emails addresses
%
% Emails with both sexes: dennisandlannawalker@hotmail.com, (man and wife?)
% Emails with both sexes: billieonair@yahoo.ca (bill and billie
%
% Remove names from masculine list that have been feminised (such as adding a 'a' to the end of a masculine name)
% 



