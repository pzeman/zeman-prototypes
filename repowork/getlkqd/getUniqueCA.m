%
%
% get unique cell array category values from category list
%

function [categoryList] = getUniqueCA(categoryRaw)

categoryRaw = upper(categoryRaw);
%size(categoryRaw)
%For each entry in the Raw, get the indices of common entries and then remove them from the Raw.  Then repeat until complete


%%%% FIRST GET UNIQUE STRINGS FOR THE CURRENT INPUT

cnt = 1;
while size(categoryRaw,2) > 0
  thisCategory = upper(char(categoryRaw{1})); %get category string
  categoryList{cnt} = thisCategory;
  cnt = cnt+1;
  I = find(ismember(categoryRaw,thisCategory)); %find all occurrances
  categoryRaw(I) =[]; %remove all occurances
end 
 
 
 %categoryList(cnt) = {'hell2'};
 
 
%%%%%%%%%%%%%%%%%%
%%%%% COMPARE THE NEW UNIQUE STRINGS TO THE STORED HISTORY AND APPEND ANY NEW STRINGS TO THE BOTTOM

if ~exist('uniquelisthistory.mat')
   %  save('uniquelisthistory.mat','categoryList','-V7');
   return
end
 

 
fprintf('Loading history\r\n');
dat = load('uniquelisthistory.mat');

%remove from the new categoryList what is stored in the old one

%size(dat.categoryList)



for i = 1:size(dat.categoryList,2)
  I = strmatch(upper(char(dat.categoryList(i))), upper(char(categoryList)),'exact');
  %I
  categoryList(I) =[];
end



categoryList

if size(categoryList,2) > 0
  fprintf('Appending new unique entries\r\n');
  cnt = 1;
  for k = size(dat.categoryList,2)+1: size(dat.categoryList,2) + size(categoryList,2)
    dat.categoryList(k)= categoryList{cnt};
    cnt = cnt +1;
  end
end
  

categoryList = dat.categoryList;
 
 
%%% SAVE UPDATED VERSION
%if ~exist('uniquelisthistory.mat')
     save('uniquelisthistory.mat','categoryList','-V7');
  
%end
 
 
 
 
 
 

