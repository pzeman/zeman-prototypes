%
%
% Plot the stored results of polls to the lqkd interface
%

function [thisSupplierMean, thisTimeSegment, tempmtx, tagList] = plotlkqd(supplierIdx,VARPFLAG)


%supplierString = 'thispoint_YEPMEDIA-2.25IA';

VARP = VARPFLAG; %0 = volume, 1 = adrate


PLOTDETAILFLAG = 0;
PLOTAVGFLAG = 1;


supplierStringList = {'thispoint_ADGORITHMS-1.5IA','thispoint_ADGORITHMS-1.5MW','thispoint_APPALGO---3-IA',...
'thispoint_DSNR---1.5MW','thispoint_MARS-MEDIA-2.25IA','thispoint_MARS-MEDIA-2IA','thispoint_OUTBROWSE-2IA',...
'thispoint_VELIS-1.5IA','thispoint_VELIS-1.75IA','thispoint_YBRANT-1.7IA','thispoint_YEPMEDIA-2.25IA'};

supplierString = supplierStringList{supplierIdx};


%stepsize = 2;

%load('./thispoint.mat');

%%%%%%%%%%%%%%%
%% LOAD THE SUPPLIER FILE CONTAINING ON-GOING DATA

supplierFileName = [supplierString '.mat'];
fprintf('Loading %s\r\n',supplierFileName);
load(supplierFileName);
npts = size(thispoint,2);
fprintf('number time points is %i\r\n',npts);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Get a list of the tags from the "deal" variable
cnt =1;
for i = 1:npts
  nentries = length(thispoint(i).x);
  for k = 1:nentries

    %tagList{cnt} =   sprintf('%s',char(thispoint(i).st{k}));
    tagList{cnt} =   sprintf('%s',char(thispoint(i).deal{k}));
    
    cnt = cnt +1;
  end
end

%size(tagList)
tagList = getUniqueCA(tagList);
ntags = size(tagList,2);






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Assign an index to each of the tagList items

tempmtx = zeros(ntags,npts);
tempmtxDescription = {};


for i = 1:npts
  nentries = length(thispoint(i).x);
  for k = 1:nentries

    %I = strmatch( upper(char(thispoint(i).st{k})) , upper(char(tagList))  ,'exact' );
    I = strmatch( upper(char(thispoint(i).deal{k})) , upper(char(tagList))  ,'exact' );
    
%    tempmtx(I,i) = thispoint(i).y(k);  

    if VARP == 1

    tempmtx(I,i) = thispoint(i).adrate(k); 
    elseif VARP == 0
    tempmtx(I,i) = thispoint(i).volume(k); 
    end
    
    %I
    %pause
    tempmtxDescription{I} = upper(char(tagList(I)));

    

  end
end


%size(tempmtxDescription)
%tempmtxDescription{1}
%tempmtxDescription{2}


%pause


if PLOTDETAILFLAG == 1
figure; 
plot(tempmtx','LineWidth',2)
%plot(log10(tempmtx([2 8 9],:)+eps)','LineWidth',2);
hold on;
%legend(tempmtxDescription,'TextSize',2);


for i = 1:npts
  nentries = length(thispoint(i).x);
  for k = 1:nentries

    %I = strmatch( upper(char(thispoint(i).st{k})) , upper(char(tagList))  ,'exact' );
    %tempmtx(I,i) = thispoint(i).y(k);     
    if (i== 1) || (i ==npts)
       %text(i,thispoint(i).y(k),  sprintf('%s',char(thispoint(i).st{k}))  );
       if VARP == 1
       text(i,thispoint(i).adrate(k),  sprintf('%s',char(thispoint(i).deal{k}))  );    
       elseif VARP ==0
       text(i,thispoint(i).adrate(k),  sprintf('%s',char(thispoint(i).volume{k}))  );    
       end
       
    end       
  end
  
  thisTimeString = datestr(thispoint(i).timestring{1},'ddmmmHHMM');
  %tempd2 = datestr(thispoint(i).timestring{1},'ddmmmm-HH:MM')  
       h_text = text(i,0,  sprintf('%s',thisTimeString)  ,'rotation',-90);  
end
end %if PLOTDETAILFLAG









%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%% ALIGN BY SPECIFIED HOUR AS DAILY TRIALS
%i = 1;
dayLengthSamples = 24*4; 
%startTime = 1800;
startTime = 1030;


thisTimeList = [];
for i = 1:npts
thisTimeList(i) = str2num(datestr(thispoint(i).timestring{1},'HHMM'));
end


%get all index locations of the specified start time
I = find(thisTimeList == startTime);
I
if length(I) > 1
  fprintf('find more than one of the startming positoins\r\n');
  %pause
end
%pause
cnt = 1;
thisTimeSegment =[];
thisDataSegment = [];

%for dealidx = 1:size(tempmtx,1)

for i = 1:length(I)

i
  if i < length(I)
    fprintf('fit\r\n');
    thisTimeSegment(cnt,:) = thisTimeList(I(i):I(i+1)   );
   % size(tempmtx)
   % I(i)
   % I(i+1)
    
    xvec = I(i):I(i+1);
    %xvec(1)
    %xvec(end)
    thisDataSegment(cnt,:,:) = tempmtx(:,  xvec   )';
  else
    %  size( thisTimeList(I(i):end) )
     % size( thisTimeSegment(:,:) )
     % pause
      if length(I) == 1 %only do this if less than 1 day in buffer
      thisTimeSegment(cnt,:) = thisTimeList(I(i):end);
      thisDataSegment(cnt,:,:) = tempmtx(:,I(i):end  )';
      end
      
  end
  
  cnt = cnt +1;

end
%end %for dealidx


temp = mean(thisDataSegment,3);
temp = reshape(temp,size(temp,1) , size(temp,2));
thisSupplierMean = mean(temp,1);
%size(temp)

%pause



%size(thisDataSegment)
%pause
%tempstderr = std(thisDataSegment,0,3)/sqrt(size(thisDataSegment,3)-1);
%tempstderr = std(thisDataSegment,0,3);

%tempstderr = reshape(tempstderr,size(tempstderr,1) , size(tempstderr,2));

%tempstderr
%size(tempstderr)
%pause


%thisTimeSegment(1,1:10)
%thisTimeSegment(2,1:10)


%size(temp)
%size(tempstderr)

hfig_avg = figure; 


%size(temp)
%temp
%pause

plot(temp');
hold on;
%plot((temp-tempstderr)','r');
%plot((temp+tempstderr)','r');

%pause

hold on;
supplierStringTemp = strrep(supplierString,'_','-');
title(sprintf('Supplier:%s: mean across deals for this supplier starting at time: %i UTC',supplierStringTemp,startTime));
xvec = 1:length(temp);

for i = 1:length(thisTimeSegment)
hold on;
%thisTimeSegment(i)
%xvec(i)

text(xvec(i),0,sprintf('%i',thisTimeSegment(1,i)),'rotation',90);
%text(1,1,'a');

end

%pause
axis([0 max(xvec) 0 max(max(temp))]);




%figure;
%hold on;
%for i = 1:npts
%  nentries = length(thispoint(i).x);
%  fprintf('nentires for this point in time is %i\r\n',nentries);
%  for k = 1:nentries
%    plot(i,thispoint(i).y(k));
%    text(i,thispoint(i).y(k),sprintf('%s',char(thispoint(i).st{k})));
%  end
%end




