%
%
%  Plot mean and estimate of mean of the supplier's deal ad rates
%
%
supplierMean = []; 
cnt = 1; 
for k = 2:11 
[supplierMean(cnt,:) timeAxis] = plotlkqd(k); 
cnt =cnt+1; 
end

timeAxis = timeAxis(1,:); %only use 1 of the same set of vectors


%%%%%%%%
% CALC STANDARD ERROR OF THE MEAN
nSamples = size(supplierMean,1);

stdErrorOfMean = std(supplierMean,0,1)/sqrt(nSamples-1);

figure; 

subplot(2,1,1);
plot(mean(supplierMean,1),'b','LineWidth',3);
hold on;
plot(mean(supplierMean,1) + stdErrorOfMean ,'r');
plot(mean(supplierMean,1) - stdErrorOfMean ,'r');
title('mean and standard error');
xvec = 1:size(supplierMean,2);
axis([0 max(xvec) 0 max(max(supplierMean))]);
for i = 1:length(xvec)
text(xvec(i),min(min(mean(supplierMean,1))),sprintf('%i',timeAxis(i)),'rotation',-90);
end


snr = mean(supplierMean,1)  ./ std(supplierMean,0,1);
%figure; 
subplot(2,1,2);
plot(snr,'b');
axis([0 max(xvec) 0 max(snr)]);
for i = 1:length(xvec)
text(xvec(i),min(min(snr)),sprintf('%i',timeAxis(i)),'rotation',-90);
end
