%
%
% plot correlation between AdRate and Volume
%
%

CALCALLFLAG = 1;
if CALCALLFLAG == 1
supplier=[];
supplierAdRates = []; 
cnt = 1; 
TYPEFLAG = 1; % adrate
for k = 2:11 
[supplierMean(cnt,:) timeAxis supplierAdRates tagList] = plotlkqd(k,TYPEFLAG); 
supplier(cnt).AdRates = supplierAdRates;
supplier(cnt).TagList = tagList;

cnt =cnt+1; 
end

close all


supplierVolumes = []; 
cnt = 1; 
TYPEFLAG = 0; % volume
for k = 2:11 
[supplierMean(cnt,:) timeAxis supplierVolumes ] = plotlkqd(k,TYPEFLAG); 
supplier(cnt).Volumes = supplierVolumes;
cnt =cnt+1; 
end

close all

end %eof CALCALLFLAG





k = 1
%%%%%%%%%%%%%%%%%%%%%%%55
%% Calculate correlation relationships
windowsize = 4*3;
N = size(supplier(k).Volumes,1);
cntw = 1;
temp=[];
b = windowsize;
a = 1;
while b<=size(supplier(k).Volumes,2)
cnt = 1;
for i = 1:N


    cc = corrcoef( supplier(k).AdRates(i,a:b)' , supplier(k).Volumes(i,a:b)' );
    if isnan(cc)
       supplier(k).AdRates(i,a:b)
       supplier(k).Volumes(i,a:b)
%       pause
    end
    %cc
    temp(cnt,cntw) = cc;
    cnt = cnt+1;


end

cntw = cntw+1;
a = a+windowsize+1;
b = b+windowsize+1;
end %eof while


figure; imagesc(temp); colorbar;
for i = 1:size(supplier(k).TagList,2)
  text(0,i,sprintf('%s',char(supplier(k).TagList{i})));
end
title('correlations');



ss = sign(temp);
figure; imagesc(ss.*abs(atanh(temp))); colorbar;
for i = 1:size(supplier(k).TagList,2)
  text(0,i,sprintf('%s',char(supplier(k).TagList{i})));
end
title('atanh correlations mapped to approx gaussian to emphasize tails');






%pause
