%
%
% Calls bash script lkqdtest2.sh to get lkqd data
%
% Uses current HOUR for query time.
%   * Test if we should actually use the previous hour to get acurate numbers
%
% Provides simple data formatting 
%
%
function [retval] = getlkqd(dateNumberStart,dateNumberEnd)
%retval= 1;



switch nargin
    case 2
     fprintf('using specified input arguments for date of query.\r\n');   
    
    thisDateStart = datestr(dateNumberStart,'yyyy-mm-dd-HH')
    thisDateEnd = datestr(dateNumberEnd,'yyyy-mm-dd-HH')
    
     startDate = thisDateStart(1:10);
     endDate = thisDateEnd(1:10);

     startHour = thisDateStart(12:13);
     endHour = thisDateStart(12:13);
     
    
    
    otherwise
    
     %if no parameters are specified, 
     thisTimeCode = now;
    
     thisDate = datestr(thisTimeCode,'yyyy-mm-dd-HH')

     startDate = thisDate(1:10);
     endDate = startDate;

     startHour = thisDate(12:13);
     endHour = startHour;
     
end




colDemandDeal = 3;
colSupplySource = 2;
colImpressions = 7;
colProfit = 14;
colAdRate = 8;

colCpm = 11;
colEfficiency = 10;
colWins = 6;
colRequests = 4;







lkqdErrorString = 'There_was_an_internal_server_error.';

% OPTIONS for paramSecond
%
%#demand deal (the advertiser) DEAL
%#country      DEVICE
%#device       COUNTRY
%#domain       SITE
%#appname      APP_NAME
%#suppy source SITE

%Fee= impression X 0.17 / 1000
%[2:02:34 PM] Philip Michael Zeman: okay. So it is a contant value. So GL profit = Profit - Fee ?



paramSecond = 'DEAL'

systemCallString = ['./lkqdtest2.sh ' startDate ' ' endDate ' ' startHour ' ' endHour ' ' paramSecond];
fprintf('System call: %s\r\n',systemCallString);

[status, output] = system(systemCallString); %creates output file lkqdtest.csv

fprintf('** Returned from system call\r\n');
fflush(stdout);


fprintf('**Reading the file for parsing and cleaning\r\n');
fflush(stdout);
csvdata = fileread('lkqdtest.csv');


if length(csvdata) <= length(lkqdErrorString)
fprintf('Software Detected Error: data read from API too short\r\n');
return
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FORMAT DATA AND SAVE TO "CLEAN" CSV FILE

%remove double quotes, percent, and dollar signs so that we can work with numbers
csvdata = strrep(csvdata,'"','');
csvdata = strrep(csvdata,'$','');
csvdata = strrep(csvdata,'%','');
csvdata = strrep(csvdata,' ','-'); %replace spaces with _
csvdata = strrep(csvdata,'_','-'); %replace spaces with _

fid = fopen('./lkqdtestclean.csv','w');
fprintf(fid,'%s',csvdata);
fclose(fid);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% READ THE CLEAN CSV FILE AND EXTRACT INFO FOR A SPECIFIED SUPPLIER
% and each corresponding demand deal

%load the file into 
dataArray = readmixedcsvall('./lkqdtestclean.csv',',');
dataHeader = dataArray(1,:);
dataArray(1,:) = [];

%Get the number of unique entries for SUPPLIER field
temp = upper(dataArray(:,2))';
uniqueList = getUniqueCA(temp);
numberUnique = size(uniqueList,2);


sourceNameStringList = {'Adgorithms-1.5IA', 'Outbrowse-2IA','Velis-1.5IA','Velis-1.75IA','Ybrant-1.7IA',...
'Mars-Media-2IA','YepMedia-2.25IA','Mars-Media-2.25IA','Adgorithms-1.5IA','DSNR---1.5MW','appAlgo---3-IA','Adgorithms-1.5MW'};

for q = 1: length(sourceNameStringList)

%Choose the SUPPLY SOURCE to track
% find YepMedia_3IA

%sourceNameString = 'YepMedia-3IA';

sourceNameString = sourceNameStringList{q};
fprintf('Processing: *%s*\r\n',char(sourceNameString));
fflush(stdout);

thisI = strmatch(upper(sourceNameString) , uniqueList,'exact'); %find the indices of all locations this exact string is found
i = thisI;


I = strmatch( upper(char(uniqueList(i))) , upper( char(dataArray(:,2) ) ),'exact'); %indices of each




%sort deals based on adrate
[Ybest, Ibest] = sort(dataArray(I,colAdRate),'descend');
%thisBestDeal = dataArray(I(Ibest),3);
%thisBestDealAdRate = dataArray(I(Ibest),colAdRate);


thisDeal = dataArray(I,3);
thisAdRate = dataArray(I,colAdRate);
thisCpm = dataArray(I,colCpm);
thisEfficiency = dataArray(I,colEfficiency);
thisWins = dataArray(I,colWins);
thisRequests = dataArray(I,colRequests);



thisTime = thisTimeCode;

%figure(30);
%xlabel('Time (date num)');
%ylabel('log AdRate');
%storedpoint = load('./thispoint.mat');

cnt = 1
numberEntries = size(thisDeal,1);
thispoint =[];
for k = 1:numberEntries

  %st = sprintf('%s',char(thisBestDeal(k))); 
  x = thisTime;
 % y = (str2num(char(thisBestDealAdRate(k))));

  
 % deal = 
  
  %wins = 
 % efficiency =  
 % adrate = 
 % cpm = 
 % volume = 
  
  %plot(x,y,'bs');
  %hold on;
  %text(x,y,st);

  thispoint(cnt).x(k) = x;
  

  thispoint(cnt).volume(k) = (str2num(char(thisRequests(k))));
  thispoint(cnt).deal{k} = sprintf('%s',char(thisDeal(k)));
  thispoint(cnt).wins(k) = (str2num(char(thisWins(k))));
  thispoint(cnt).efficiency(k) = (str2num(char(thisEfficiency(k)))); 
  thispoint(cnt).adrate(k) = (str2num(char(thisAdRate(k))));
  thispoint(cnt).cpm(k) = (str2num(char(thisCpm(k))));
  
  thispoint(cnt).timestring{k} = thisTimeCode;

  
  %%%LEGACY: REMOVE THIS.
  thispoint(cnt).st{k} = thispoint(cnt).deal{k};
  thispoint(cnt).y(k) = thispoint(cnt).adrate(k);
  
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Write the structured data to file

thisSourceString = sprintf('thispoint_%s.mat',upper(sourceNameString));

if exist(thisSourceString);
  %append the new data to previous data if storage file exists
  %storedpoint = load('./thispoint.mat');
  storedpoint = load(thisSourceString);
  
  
  fprintf('size storedpoint.thispoint\r\n');
  npts = size(storedpoint.thispoint,2)

  %append
  storedpoint.thispoint(npts+1) = thispoint; 
  thispoint = storedpoint.thispoint;
  
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Write to appended or new data to disk


save(thisSourceString,'thispoint','-V7');




end %FOR LOOP FOR ANALYZING EACH SUPPLY SOURCE







