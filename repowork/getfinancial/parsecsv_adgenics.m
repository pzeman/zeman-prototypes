%
%
%
% Parse the adgenics styled  CSV financla data
%
% Output csvgl file which is formatted for GL
%
% Case sensitive date string: example: 02-Dec-2015
% query year string: '2015'
%

function [successFlag] = parsecsv_adgenics(queryDateString)

successFlag = 1;

queryDateString = strrep(queryDateString,'/','-');

more off
%queryDateString = '02-Dec-2015';
queryTypeString = 'clicks';
interfaceTypeString = 'adgenics';
inputFormat = 'csv';
outputpath_rawdata = '/home/abv/CLIENT_gravitylab/financialdata/';


queryYearString = queryDateString(7:10);

%queryDateString

%pause

fileNameMask = ['*' '-apitype_' interfaceTypeString '-raw_' queryDateString '*' queryTypeString '.' inputFormat];
 
fprintf('searching using mask %s\r\n',[outputpath_rawdata queryYearString '/' fileNameMask]);
 
d = dir([outputpath_rawdata queryYearString '/' fileNameMask]);
  
  
numberFiles = size(d,1);
fprintf('Found %i raw %s files\r\n',numberFiles,interfaceTypeString);
  
  
for i = 1:numberFiles
  fileName = d(i).name;
  parsecsv_aux(outputpath_rawdata, queryYearString, fileName)
end







%%%%%%%%%%%%%%%%AUX FUNCTIONS %%%%%%%%%%%%%%%%%%




function [] = parsecsv_aux(outputpath_rawdata, queryYearString, fileName)
%DEFINEJAVA PATH FOR PARSING XML
  %javaaddpath('/opt/xerces-2_11_0/xml-apis.jar');
  %javaaddpath('/opt/xerces-2_11_0/xercesImpl.jar');




%  queryYearString = '2015/'
queryYearString = [queryYearString '/'];
%  queryYearString = [queryYearString '/'];
%  outputpath_rawdata = '/home/abv/CLIENT_gravitylab/financialdata/';
%fileName = 'affiliates_clickbooth_com-raw_02-Dec-2015-154957_CampaignSummary.xml';

%get filename date string
I1 = strfind(fileName,'raw_');
%I2 = strfind(fileName,'_CampaignSummary);
fileNameDateString = fileName(I1+4:I1+4+17);

I1 = strfind(fileName,'-raw');
fileNamePartnerString = fileName(1:I1-1);

%fileNamePartnerString

%fileNameDateString


inputFileNameRoot = fileName(1:end-4);
outputFileName = [inputFileNameRoot '.csvgl']; %muse have different output extension because input is also csv

filePathName =[outputpath_rawdata queryYearString fileName];
outputFilePathName = [outputpath_rawdata queryYearString outputFileName];
 
 
 
 
 %%%%%%%%%%%%%%%%%%%%%5 
%%LOAD THE INFO FILE FOR THIS PARTNER
I = strfind(fileName,'-apitype');
fileName_info = [outputpath_rawdata queryYearString fileName(1:I-1) '-info.txt'];

fprintf('Loading and parsing info file: %s\r\n',fileName_info);
[info, successflag]= oc_parsefile(fileName_info);

info

 business_segment = info.businessSegment;
 business_segment_child = info.businessSegmentChild;
 transaction_type = info.transactionType
 gravitylab_id = info.GLID;
 partner_name = info.partnerName;
  query_date = info.queryDate;
 
 
 
 
 lineArray = readmixedcsvall(filePathName,',');
 fprintf('Completed reading %s \r\n',filePathName);
 
 %ans = campaignid
%ans = date
%ans = c1
%ans = c2
%ans = c3
%ans = ip
%ans = amount
 
% size(lineArray)
Nentries = size(lineArray,1);

writeString =[];
cnt = 1;
for i = 2:Nentries %SKIP THE FIRST LINE (HEADER) BY STARTING WITH 2
  lineArray{1,:}
  lineArray{2,:}
 
  blank = ' ';
  %revenue = lineArray{i,7};
  transaction_amount = lineArray{i,7}; %amount
  
  %fileNameDateString %above
  %fileNamePartnerString %above
  partner_id = ' ';
  offer_id = lineArray{i,1}; %campaignid
  offer_name = ' ';
  subid1 = lineArray{i,3};
  subid2 = lineArray{i,4};
  subid3 = lineArray{i,4};
  
  transaction_date = lineArray{i,2}; %date
  ip_address = lineArray{i,6}; %ip
  
  
   vertical_name = ' ';
   offer_details=' ';
   offer_name = ' ';
   impressions = ' ';
   price = ' ';
   price_format = ' ';
   click_id =' ';
   %subid1=' ';
   %subid2=' ';
   %subid3=' ';
   currency_symbol= ' ';
   conversion_rate = ' ';
   conversions = ' ';
   thisrpm =' ';
   epc = ' ';
   clicks = ' ';
    
    
    
    
    
    
    
  
  
 % writeString{i} = [blank ',' blank ',' blank ',' revenue ',' fileNameDateString ',' fileNamePartnerString ',' partner_id ',' ...
 %    offer_id ',' offer_name ',' offer_details ',' vertical_name ',' price_format ',' price ',' impressions ',' click_id ',' ...
 %    clicks ',' conversions ',' ip_address ',' subid1 ',' subid2 ',' subid3 ',' conversion_rate ',' currency_symbol ',' thisrpm ',' epc];

     
     
 %  writeString{cnt} = [business_segment ',' business_segment_child ',' transaction_type ',' transaction_amount ',' fileNameDateString ',' ...
 %        gravitylab_id ',' partner_name ',' fileNamePartnerString ',' partner_id ',' ...
 %        offer_id ',' offer_name ',' offer_details ',' vertical_name ',' price_format ',' price ',' impressions ',' click_id ',' ...
 %        clicks ',' conversions ',' ip_address ',' subid1 ',' subid2 ',' subid3 ',' conversion_rate ',' currency_symbol ',' thisrpm ',' epc];

     
     
       writeString{cnt} = [query_date ',' transaction_date ',' business_segment ',' business_segment_child ',' transaction_type ',' transaction_amount ',' fileNameDateString ',' ...
         gravitylab_id ',' partner_name ',' fileNamePartnerString ',' partner_id ',' ...
         offer_id ',' offer_name ',' offer_details ',' vertical_name ',' price_format ',' price ',' impressions ',' click_id ',' ...
         clicks ',' conversions ',' ip_address ',' subid1 ',' subid2 ',' subid3 ',' conversion_rate ',' currency_symbol ',' thisrpm ',' epc];

     
     
     
     
     cnt = cnt+1;
 end
 
 

%filename = 'sample.xml';
 
 % These 3 lines are equivalent to xDoc = xmlread(filename) in matlab
 
%global parser
%global xDoc

 %parser = javaObject('org.apache.xerces.parsers.DOMParser');
 %parser.parse(filePathName); 
 %xDoc = parser.getDocument;
 


%FIRST GET ROW COUNT SO WE KNOW HOW MANY ENTRIES



 % get first data element 
 %elem = xDoc.getElementsByTagName('row_count').item(0); %first item with tag row_count
 % get text from child
 %dataString = elem.getFirstChild.getTextContent;
 %data = str2num(dataString);
 %numberEntries = data; %indexed in the xml 0 to 75-1

%%%%% READ ALL DATA

%cnt = 1
%writeString =[];
%for idx = 0:numberEntries-1

   %elem = xDoc.getElementsByTagName('offer_id').item(idx); %first item with tag row_count
   %offer_idString = elem.getFirstChild.getTextContent;
   
   %offer_id = getinfo('offer_id',idx);
   %vertical_name = getinfo('vertical_name',idx);
   %price_format = getinfo('price_format',idx);
   %price = getinfo('price',idx);
   %impressions = getinfo('impressions',idx);
   %clicks = getinfo('clicks',idx);
   %conversions = getinfo('conversions',idx);
   %conversion_rate = getinfo('conversion_rate',idx);
   %currency_symbol = getinfo('currency_symbol',idx);
   %revenue = getinfo('revenue',idx);
   %epc = getinfo('epc',idx);

   %blank = ' ';
   %partner_id = ' ';
   %offer_name=' ';
   %offer_details=' ';
   %click_id =' ';
   %ip_address = ' ';
   %subid1=' ';
   %subid2=' ';
   %subid3=' ';
   %thisrpm =' ';

  % writeString{cnt} = [blank ',' blank ',' blank ',' revenue ',' fileNameDateString ',' fileNamePartnerString ',' partner_id ',' offer_id ',' offer_name ',' offer_details ',' vertical_name ',' price_format ',' price ',' impressions ',' click_id ',' clicks ',' conversions ',' ip_address ',' subid1 ',' subid2 ',' subid3 ',' conversion_rate ',' currency_symbol ',' thisrpm ',' epc];

%   writeString{cnt} = ' ';

%cnt = cnt +1;
%end %eof for





WRITEOUTPUTFLAG = 1;

if WRITEOUTPUTFLAG == 1




fprintf('completed reading CSV. Now writing to file.\r\n');


%size(writeString)
numberEntries_writeString = size(writeString,2);
fprintf('Opening : *%s* for writing\r\n',outputFilePathName);

fid = fopen(outputFilePathName,'w');

%check if file opened correctly
if fid >= 1
  fprintf('file opened successfully for writing\r\n');
else 
  fprintf('Error opening file for writing %s. Aborting.\r\n',outputFilePathName);
  fclose(fid); %surrender handle
  return;
end

%write header: DO NOT WRITE HEADER IF BATCH THIS FUNCTION
%fprintf(fid,'offer_id,vertical_name,price_format,price,impressions,clicks,conversions,conversion_rate,currency_symbol,revenue,epc\n');


for cnt = 1:numberEntries_writeString
  fprintf(fid,'%s \n',char(writeString{cnt})); %write for Windows only use cr
end  
fclose(fid);




end %eof IFWRITEOUTPUTFLAG

%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%%%%%%%%%%%% AUX FUNCTIONS


%function [dataString] = getinfo(tagString,idx)
%
%
 % global parser
 % global xDoc
%
 %  elem = xDoc.getElementsByTagName(tagString).item(idx); %first item with tag row_count
 %  dataString = elem.getFirstChild.getTextContent;






 % get first data element 
% elem = xDoc.getElementsByTagName('campaign').item(0);
 % get text from child
% data = elem.getFirstChild.getTextContent
 % get attribute named att
% att = elem.getAttribute('att')
