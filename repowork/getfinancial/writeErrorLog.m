%
%
%
% Write to errors log so that we can see which calls fail
%
%

function [successFlag] = writeErrorLog(q,messageString)
successFlag = 0;

global errorFilePathName

try
%Write errors to file
fidErrors = fopen(errorFilePathName,'a');
fprintf(fidErrors,'%s : %s\r\n',q.siteUrlString,messageString);
fclose(fidErrors); 

%Write errors to standard output
fprintf('%s : %s\r\n',q.siteUrlString,messageString);
catch
  fprintf('Failed to write error to %s\r\n',errorFilePathName);
  successFlag = 0;
end

successFlag = 1;