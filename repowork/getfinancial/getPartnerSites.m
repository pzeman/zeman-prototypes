%
%
%
% Return a specific type of partner site to the calling function
%
%
%

function [qSubList] = getPartnerSites(interfaceTypeSearchString)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%% SITE DEFINITIONS BELOW

 
 q(1).queryTypeString = 'CampaignSummary';
 q(1).requestTypeString = 'get';
 q(1).interfaceType = 'cake';
 q(1).siteUrlString = 'login.addemand.com'; %works
 q(1).apiKey = '7YHOLy9mze2DgiajccYZA';
 q(1).affiliateId = '14708';
 q(1).partnerName = 'addemand.com';
 q(1).outputFormat = 'xml';
 q(1).transactionType = 'revenue';
 q(1).businessSegment ='email';
 q(1).businessSubsegment = '';
 q(1).callFrequencyMinutes = 0; 
 q(1).businessSegment = 'email';
 q(1).businessSegmentChild = 'default';
 q(1).transactionType = 'default';
 q(1).GLID = 'GL00002';
 
 
 q(2).queryTypeString ='CampaignSummary';
 q(2).requestTypeString = 'get';
 q(2).interfaceType = 'cake';
 q(2).siteUrlString = 'affiliates.clickbooth.com'; %works
 q(2).apiKey = 'YUgMLxLLVkTzSf8MzqEug';
 q(2).affiliateId = '510882';
 q(2).partnerName = 'clickbooth.com';
 q(2).outputFormat = 'xml';
 q(2).transactionType = 'revenue';
 q(2).businessSegment ='email';
 q(2).businessSubsegment = '';
 q(2).callFrequencyMinutes = 0;
 q(2).businessSegment = 'email';
 q(2).businessSegmentChild = 'default';
 q(2).transactionType = 'default';
 q(2).GLID = 'GL00007'; 
 
 
 q(3).queryTypeString ='CampaignSummary';
 q(3).requestTypeString = 'get';
 q(3).interfaceType = 'cake';
 q(3).siteUrlString = 'login.concisemediagroup.com'; %works
 q(3).apiKey = 'MqzQZlZpURs';
 q(3).affiliateId = '203';
 q(3).partnerName = 'concisemediagroup.com';
 q(3).outputFormat = 'xml';
 q(3).transactionType = 'revenue';
 q(3).businessSegment ='email';
 q(3).businessSubsegment = '';
 q(3).callFrequencyMinutes = 0; 
 q(3).businessSegment = 'email';
 q(3).businessSegmentChild = 'default';
 q(3).transactionType = 'default';
 q(3).GLID = 'GL00011';
 
 
 q(4).queryTypeString ='CampaignSummary';
 q(4).requestTypeString = 'get';
 q(4).interfaceType = 'cake';
 q(4).siteUrlString = 'usmoffers.com'; %works
 q(4).apiKey = 'dJjKOh62pCo';
 q(4).affiliateId = '24';
 q(4).partnerName = 'usmoffers.com';
 q(4).outputFormat = 'xml';
 q(4).transactionType = 'revenue';
 q(4).businessSegment ='email';
 q(4).businessSubsegment = '';
 q(4).callFrequencyMinutes = 0; 
 q(4).businessSegment = 'email';
 q(4).businessSegmentChild = 'default';
 q(4).transactionType = 'default';
 q(4).GLID = 'GL00019';
 
 q(5).queryTypeString ='CampaignSummary';
 q(5).requestTypeString = 'get';
 q(5).interfaceType = 'cake';
 q(5).siteUrlString = 'affiliates.mate1inc.com'; %works
 q(5).apiKey = 'e5bWxRRgypQ';
 q(5).affiliateId = '464';
 q(5).partnerName = 'mate1inc.com';
 q(5).outputFormat = 'xml';
 q(5).transactionType = 'revenue';
 q(5).businessSegment ='email';
 q(5).businessSubsegment = '';
 q(5).callFrequencyMinutes = 0; 
 q(5).businessSegment = 'email';
 q(5).businessSegmentChild = 'default';
 q(5).transactionType = 'default';
 q(5).GLID = 'GL00031';
 
 
 
 q(6).queryTypeString ='CampaignSummary';
 q(6).requestTypeString = 'get';
 q(6).interfaceType = 'cake'; %THIS TIMES OUT If doing the large query.
 q(6).siteUrlString = 'login.affluentco.com'; % WORKS %networks@gavitylab.com
 q(6).apiKey = 'wYtt4nW2ytE0HCJbRe6Q7g';
 q(6).affiliateId = '201686';
 q(6).partnerName = 'affluentco.com';
 q(6).outputFormat = 'xml';
 q(6).transactionType = 'revenue';
 q(6).businessSegment ='email';
 q(6).businessSubsegment = '';
 q(6).callFrequencyMinutes = 0; 
 q(6).businessSegment = 'email';
 q(6).businessSegmentChild = 'default';
 q(6).transactionType = 'default';
 q(6).GLID = 'GL00059';
 
 
 q(7).queryTypeString = 'CampaignSummary';
 q(7).requestTypeString = 'get';
 q(7).interfaceType = 'cake';
 q(7).siteUrlString = '7roi.net'; %works
 q(7).apiKey = 'tpL2u40ZNww';
 q(7).affiliateId = '1439';
 q(7).partnerName = '7roi.net';
 q(7).outputFormat = 'xml';
 q(7).transactionType = 'revenue';
 q(7).businessSegment ='email';
 q(7).businessSubsegment = '';
 q(7).callFrequencyMinutes = 0; 
 q(7).businessSegment = 'email';
 q(7).businessSegmentChild = 'default';
 q(7).transactionType = 'default';
 q(7).GLID = 'GL00061';
 
 
 %GET CSV and PARSE TO CSVGL

 q(8).queryTypeString = 'clicks'; %clicks, sales, or test
 q(8).requestTypeString = 'get';
 q(8).interfaceType = 'adgenics';
 q(8).siteUrlString = 'reporting.gwmtracker.com';
 q(8).apiKey = 'cad59c17469c3cf4ca9d9b6ed19c8c7dae74668cf2ac489b';
 q(8).affiliateId = '3200064'; %key not used for api call
 q(8).partnerName = 'gwmtracker.com';
 q(8).outputFormat = 'csv';
 q(8).transactionType = 'revenue';
 q(8).businessSegment ='email';
 q(8).businessSubsegment = '';
 q(8).callFrequencyMinutes = 15; 
 q(8).businessSegment = 'email';
 q(8).businessSegmentChild = 'default';
 q(8).transactionType = 'default';
 q(8).GLID = 'GL00017';
 
 q(9).queryTypeString = 'clicks'; %clicks, sales, or test
 q(9).requestTypeString = 'get';
 q(9).interfaceType = 'adgenics';
 q(9).siteUrlString = 'reporting.agstracker.com';
 q(9).apiKey = 'e47123271efbff107b1206bad3b0f14d8d5148c4bf0245f26290f110e97aa226';
 q(9).affiliateId = '350851'; %key not used for api call
 q(9).partnerName = 'agstracker.com';
 q(9).outputFormat = 'csv';
 q(9).transactionType = 'revenue';
 q(9).businessSegment ='email';
 q(9).businessSubsegment = '';
 q(9).callFrequencyMinutes = 0; 
 q(9).businessSegment = 'email';
 q(9).businessSegmentChild = 'default';
 q(9).transactionType = 'default';
 q(9).GLID = 'GL00003';
 
 
 q(10).queryTypeString = 'clicks'; %clicks, sales, or test
 q(10).requestTypeString = 'get';
 q(10).interfaceType = 'adgenics';
 q(10).siteUrlString = 'reporting.trk4.com';
 q(10).apiKey = '2957a170f23bf026a295e6d5b090a74b13a39f494a0de1d9';
 q(10).affiliateId = '107013'; %key not used for api call
 q(10).partnerName = 'w4.com';
 q(10).outputFormat = 'csv';
 q(10).transactionType = 'revenue';
 q(10).businessSegment ='email';
 q(10).businessSubsegment = '';
 q(10).callFrequencyMinutes = 0; 
 q(10).businessSegment = 'email';
 q(10).businessSegmentChild = 'default';
 q(10).transactionType = 'default';
 q(10).GLID = 'GL00053';
 
 
 q(11).queryTypeString = 'default'; %clicks, sales, or test
 q(11).requestTypeString = 'get';
 q(11).interfaceType = 'interlincx';
 q(11).siteUrlString = 'thankyoupath.interlincx.com';
 q(11).apiKey = 'ad3c632b315179067a6defa5e2b592cddba7f3e699a6a7571fd39b3207dcd18f';
 q(11).affiliateId = '1867'; %key not used for api call
 q(11).partnerName = 'interlincx.com'; %not used by API but is used for GL reporting
 q(11).outputFormat = 'csv';
 q(11).transactionType = 'revenue';
 q(11).businessSegment ='email';
 q(11).businessSubsegment = '';
 q(11).resultErrorString(1).mask = 'date,name,clicksTotal,impressionsUnique,impressionsTotal,revenue'; %no data
 q(11).callFrequencyMinutes = 0; 
 q(11).businessSegment = 'email';
 q(11).businessSegmentChild = 'default';
 q(11).transactionType = 'default';
 q(11).GLID = 'GL00047';
 
 q(12).queryTypeString = 'clicks'; %clicks, sales, or test
 q(12).requestTypeString = 'get';
 q(12).interfaceType = 'adgenics';
 q(12).siteUrlString = 'reporting.adgtracker.com';
 q(12).apiKey = '398c777ea7081b1f081715b887f928e7e26e8144bc3781202104ca0e25f4a5f2';
 q(12).affiliateId = '331730'; %key not used for api call
 q(12).partnerName = 'flexmg.com';
 q(12).outputFormat = 'csv';
 q(12).transactionType = 'revenue';
 q(12).businessSegment ='email';
 q(12).businessSubsegment = '';
 q(12).callFrequencyMinutes = 0; 
 q(12).businessSegment = 'email';
 q(12).businessSegmentChild = 'default';
 q(12).transactionType = 'default';
 q(12).GLID = 'GL00013';
 
 
 q(13).queryTypeString = 'getcampaigns';
 q(13).requestTypeString = 'post';
 q(13).interfaceType = 'transparentads';
 q(13).siteUrlString = 'www.clrtracker.com'; %works
 q(13).apiKey = '48f30ec57ae60af23216003eb244e7c675e3ed76b165c66356d1223fc7e7acd7';
 q(13).affiliateId = '';
 q(13).securtyString = 'https://';
 q(13).partnerName = 'clrtracker.com';
 q(13).outputFormat = 'xml';
 q(13).businessSegment ='email';
 q(13).businessSubsegment = '';
 q(13).callFrequencyMinutes = 30; 
 q(13).businessSegment = 'email';
 q(13).businessSegmentChild = 'default';
 q(13).transactionType = 'default';
 q(13).GLID = 'GL00067';
 
 
 q(14).queryTypeString = 'default';
 q(14).requestTypeString = 'post';
 q(14).interfaceType = 'ytz'; 
 q(14).siteUrlString = 'api.ytz.com'; 
 q(14).apiKey = '33de86289852364d425916ed84f0c050';
 q(14).userNameString = 'networks@gravitylab.com';
 q(14).affiliateId = '';
 q(14).securtyString = 'http://';
 q(14).partnerName = 'ytz.com';
 q(14).outputFormat = 'csv';
 q(14).transactionType = '';
 q(14).businessSegment ='email';
 q(14).businessSubsegment = '';
 q(14).callFrequencyMinutes = 0; %currently not used, min mins delay required by web site API for "realtime" calls.
 q(14).businessSegment = 'email';
 q(14).businessSegmentChild = 'default';
 q(14).transactionType = 'default';
 q(14).GLID = 'GL00029';
 
 
 %Still does not seem to work as of 18DEC2015. Sent an email for tech support
 %Tech support reply is that for this to work GL must be a revshare partner.
 q(15).queryTypeString = 'default'; %clicks, sales, or test
 q(15).interfaceType = 'adnet';
 q(15).requestTypeString = 'get';
 q(15).siteUrlString = 'www.adnet.com';
 q(15).apiKey = '';
 q(15).affiliateId = ''; %key not used for api call
 q(15).partnerName = 'adnet.com';
 q(15).outputFormat = 'csv';
 q(15).userNameString = 'dan.howard@gravitylab.com';
 q(15).passwordString = 'W_x9@-U-W-.uZM_-';
 q(15).securtyString = 'https://';
 q(15).resultErrorString(1).mask = '{"status":0,"message":"Report for * is not available yet"}';
 q(15).resultErrorString(2).mask = '{"status":0,"message":"No stats found"}';
 q(15).callFrequencyMinutes = 0;
 q(15).businessSegment = 'email';
 q(15).businessSegmentChild = 'default';
 q(15).transactionType = 'default';
 q(15).GLID = 'GL00037';
 
 
 
 q(16).queryTypeString = 'CampaignSummary';
 q(16).requestTypeString = 'get';
 q(16).interfaceType = 'cake';
 q(16).siteUrlString = 'login.a4d.com'; %works
 q(16).apiKey = 'pwEFl959hsmomDO4lTIYQ';
 q(16).affiliateId = '478123';
 q(16).partnerName = 'a4d.com';
 q(16).outputFormat = 'xml';
 q(16).transactionType = 'revenue';
 q(16).businessSegment ='social';
 q(16).businessSubsegment = '';
 q(16).callFrequencyMinutes = 0; 
 q(16).businessSegment = 'social';
 q(16).businessSegmentChild = 'default';
 q(16).transactionType = 'default';
 q(16).GLID = 'GL00101';
 
 
 
 q(17).queryTypeString = 'CampaignSummary';
 q(17).requestTypeString = 'get';
 q(17).interfaceType = 'cake';
 q(17).siteUrlString = 'login.strawhouselabs.com'; %works
 q(17).apiKey = 'MSSheZ5fnNU';
 q(17).affiliateId = '15';
 q(17).partnerName = 'strawhouselabs.com';
 q(17).outputFormat = 'xml';
 q(17).transactionType = 'revenue';
 q(17).businessSegment ='social';
 q(17).businessSubsegment = '';
 q(17).callFrequencyMinutes = 0; 
 q(17).businessSegment = 'social';
 q(17).businessSegmentChild = 'default';
 q(17).transactionType = 'default';
 q(17).GLID = 'GL00097';
 
 q(18).queryTypeString = 'CampaignSummary';
 q(18).requestTypeString = 'get';
 q(18).interfaceType = 'cake';
 q(18).siteUrlString = 'network.globalwidemedia.com'; %works
 q(18).apiKey = 'CdacpKprytMNS89tiWtQQ';
 q(18).affiliateId = '536585';
 q(18).partnerName = 'globalwidemedia.com';
 q(18).outputFormat = 'xml';
 q(18).transactionType = 'revenue';
 q(18).businessSegment ='social';
 q(18).businessSubsegment = '';
 q(18).callFrequencyMinutes = 0; 
 q(18).businessSegment = 'social';
 q(18).businessSegmentChild = 'default';
 q(18).transactionType = 'default';
 q(18).GLID = 'GL00089';
 
 
 %Has fprintferror!?!?
 %q(19).queryTypeString = 'getcampaigns';
 %q(19).requestTypeString = 'post';
 %q(19).interfaceType = 'transparentads';
 %q(19).siteUrlString = 'www.drivotracker.com'; 
 %q(19).apiKey = '398c777ea7081b1fa3dc22942b5eb7c3c717c555b431fde582f338c67f9fcb58';
 %q(19).affiliateId = '';
 %q(19).securtyString = 'https://';
 %q(19).partnerName = 'drivotracker.com';
 %q(19).outputFormat = 'xml';
 %q(19).businessSegment ='email';
 %q(19).businessSubsegment = '';
 %q(19).callFrequencyMinutes = 0; 
 %q(19).businessSegment = 'email';
 %q(19).businessSegmentChild = 'default';
 %q(19).transactionType = 'default';
 %q(19).GLID = 'GL00023';
 

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% FOR THE DEFINED SITES, SEARCH FOR SPECIFIED TYPE
 
numberSites = size(q,2);

fprintf('There are %i sites available in structure.\r\n',numberSites);

fprintf('Searching for %s\r\n',interfaceTypeSearchString);
cnt = 1;
for i = 1:numberSites

  if strcmp(upper(q(i).interfaceType),upper(interfaceTypeSearchString)) == 1

    qSubList(cnt) = q(i);
    cnt = cnt+1;

  end %eof if


end




 
 
