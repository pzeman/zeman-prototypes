#!/usr/bin/env bash
#######
# add the daily summary file to the cumulative total.  This should run just before the day change PST time.
#

echo $(date) >> /home/abv/financialdatapull_adddailytocumulativetotal.log

cd ~/CLIENT_gravitylab/technical/repowork/getfinancial/
/opt/octave4.0/bin/octave ~/CLIENT_gravitylab/technical/repowork/getfinancial/octavecommandfile_cumulative.txt -no-gui --persist --interactive
