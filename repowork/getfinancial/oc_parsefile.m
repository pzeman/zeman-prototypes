%
%
% Parse the user file
%
% returns all values as strings. They must be converted to numbers as
% appropriate later on.

function [prefs, successflag]= oc_parsefile(filepathname)
prefs =[];
% Go through the list of text and get a value for each.
successflag = 1;
%fieldlist = {'software=','hardware=','srate=','numchan=','reftype=','linenoisefreq=','cleansel=','extalgor=','donate='};

%N = size(fieldlist,2); %number of fields

%test to see if we can open the file

    fid = fopen(filepathname,'r');
    if fid == -1 %if successfully opened
      successflag = 0;
      prefs =[];
      fprintf('ned_parsefile: Error Opening File: %s\n',filepathname);
      return;
    end
    
    %file opened successfully. Read the contents. Parse the ; in the file
    tline = fgetl(fid);
    fclose(fid);
    
    % arrange 
    fileitem ={};
    I = strfind(tline,';'); %find delimiters
    Nj = length(I); %number of deliminters = number of items

    for n = 1:Nj
        
        if n == 1
            a = 1;
        else
        a = I(n-1);
        end
        b = I(n);
        temp = tline(a:b);
        II = strfind(temp,';');
        temp(II) = [];
        fileitem{n} = temp;
 
    end

    
    
    %instead of searching for specific entries, make variables
for j = 1:Nj
    temp = fileitem{j};
    I = strfind(temp,'=');
    tempa=temp(1:I-1);
    tempb = temp(I+1:end);
    prefs.(genvarname(tempa)) = char(tempb);
end
    
    
    
    
    
%     
%     % filter through the contents and assign variables
%     for n = 1:N
%        
%         found = 0;
%         j = 1;
%         while (found == 0) && (j< Nj)
%             fprintf('searching for: %s\n',char(fieldlist(n))');
%             currentval = fileitem{j}
%             
%             pause
%            if  strcmp(char(fieldlist(n)),char(currentval))
%               %found. Assign to variable 
%                fprintf('FOUND: %s\n',char(fieldlist(n)));
%            end
%            j = j+1;
%         
%         end
%         
%         
%     end
%     
    
    

