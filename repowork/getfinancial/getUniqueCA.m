%
%
% get unique cell array category values from category list
%

function [categoryList] = getUniqueCA(categoryRaw)


%For each entry in the Raw, get the indices of common entries and then remove them from the Raw.  Then repeat until complete

cnt = 1;
while size(categoryRaw,2) > 0



thisCategory = upper(char(categoryRaw{1})); %get category string
categoryList{cnt} = thisCategory;
cnt = cnt+1;
 I = find(ismember(categoryRaw,thisCategory)); %find all occurrances
categoryRaw(I) =[]; %remove all occurances

%fprintf('looping\r\n');
%size(categoryRaw)
pause(0);



end 
 
  



