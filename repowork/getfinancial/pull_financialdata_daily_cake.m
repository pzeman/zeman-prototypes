%
%%
%
%
%
% Gets data for the previous day. (Start and End dates = today - 1 day.
%
% Designed to be executed daily at 5am Pacific time by CROND
%
% Dates are currently specified such that the start and end date in a range query
% are the same day.  It needs to be investigated further if any of these APIs require
% date "encapsulization" rather than "inclusive" date queiries
%
%
%


function [resultString] = pull_financialdata_daily_cake()

resultString=[]; %return empty on error



dateFormatOut = 'mm/dd/yyyy';

%get this days date and form to string MM/DD/YYYY
todayDateNum = floor(now); %the number of days between today and Jan1, 0000

%create an "inclusive" start and end date query
yesterdayDateNum = todayDateNum-1;
%todayDateNum = yesterdayDateNum; %USE SAME START AND END DATE. GET DATA FROM PREV DAY

%transform date nums to a standard date string
endDateString = datestr(todayDateNum,dateFormatOut);
startDateString = datestr(yesterdayDateNum,dateFormatOut);


%%%%%%%%%%%%%%%%%%%%%%%
% POLL SITES AND STORE RAW DATA
% Call function that queries remote web sites and saves the raw query output as XML OR CSV files
% to be further processed in a subsequent step.
[resultString] = pull_financialdata(startDateString,endDateString,'cake'); 
[resultString] = pull_financialdata(startDateString,endDateString,'transparentads');

[resultString] = pull_financialdata(startDateString,endDateString,'ytz');
[resultString] = pull_financialdata(startDateString,endDateString,'interlincx');


[resultString] = pull_financialdata(startDateString,endDateString,'adgenics'); %TO TEST



%%%%%%%%%%%%%%%%%%%%%%
% CONVERT DATA FROM EACH SITE TO CSVGL FORMAT
%
%convert to date string with - instead of /
%startDateString = strrep(startDateString,'/','-');

[successFlag] = parsexml_cake(startDateString);
[successFlag] = parsexml_transparentads(startDateString);
[successFlag] = parsecsv_ytz(startDateString);
[successFlag] = parsecsv_interlincx(startDateString);

[successFlag] = parsecsv_adgenics(startDateString); %TO TEST



%%%%%%%%%%%%
% COMBINE data from similar API-types into single CSV FILES
%
dateNum = ceil(now*100000); %use a common date num for all these so that they can be syncronized for plotting purposes
outputFileName =[];
[successFlag,outputFileName{1}] = combine_csv(startDateString,'cake',dateNum);
[successFlag,outputFileName{2}] = combine_csv(startDateString,'transparentads',dateNum);
[successFlag,outputFileName{3}] = combine_csv(startDateString,'ytz',dateNum);
[successFlag,outputFileName{4}] = combine_csv(startDateString,'interlincx',dateNum);
[successFlag,outputFileName{5}] = combine_csv(startDateString,'adgenics',dateNum);

todayDateString = datestr(now,'mm-dd-yyyy');
dailySummaryAllFilePathName = ['/home/abv/CLIENT_gravitylab/financialdata/gravitylab/livedata/hourly/' sprintf('summary_%s.csv',todayDateString)  ]; %follow symb link to samba share


%%%%%%%%%%%%
% COMBINE CSV DATA FROM ALL API TYPES INTO SINGLE CSV FILE
stAll = [];
for i = 1:size(outputFileName,2)
 st = fileread(char(outputFileName{i}));
 stAll = [stAll st];
end
fid = fopen(dailySummaryAllFilePathName,'w');
fprintf(fid,'%s',stAll);
fclose(fid);




%%%%%%%%%%%%%
% Create a cumulative daily total file
combine_dailycsv2total();




%%%%%%%%%%%%%%%%%%%%%
%
% Create Plots and email summary
CREATEPLOTSFLAG = 0;
if CREATEPLOTSFLAG == 1

[successFlag] = plotDailySummary(outputFileNameCake,startDateString);
[successFlag] = plotDailySummary(outputFileNameTransparentads,startDateString);
[successFlag] = plotDailySummary(outputFileNameYtz,startDateString);
[successFlag] = plotDailySummary(outputFileNameInterlincx,startDateString);
[successFlag] = plotDailySummary(outputFileNameAdgenics,startDateString);

end




