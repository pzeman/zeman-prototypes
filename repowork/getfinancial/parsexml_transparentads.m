%
%
%
% Parse the cake based xml financla data
%
%
% Case sensitive date string: example: 02-Dec-2015
% query year string: '2015'
%

function [successFlag] = parsexml_transparentads(queryDateString)

successFlag = 1;

queryDateString = strrep(queryDateString,'/','-');

%queryYearString = '2015';
%queryDateString = '02-Dec-2015';
queryTypeString = '_getcampaigns';
interfaceTypeString = 'transparentads';
inputFormat = 'xml';

%queryYearString = queryDateString(8:11);
queryYearString = queryDateString(7:10);
outputpath_rawdata = '/home/abv/CLIENT_gravitylab/financialdata/';


fileNameMask = ['*' '-apitype_' interfaceTypeString '-raw_' queryDateString '*' queryTypeString '.' inputFormat];
 
 fprintf('searching using mask %s\r\n',[outputpath_rawdata queryYearString '/' fileNameMask]);
 
d = dir([outputpath_rawdata queryYearString '/' fileNameMask]);
  
  
numberFiles = size(d,1);
fprintf('Found %i raw cake files\r\n',numberFiles);
  
  
for i = 1:numberFiles
  fileName = d(i).name;
  parsexml_aux(outputpath_rawdata, queryYearString, fileName)
end








%%%%%%%%%%%%%%%%AUX FUNCTIONS %%%%%%%%%%%%%%%%%%




function [] = parsexml_aux(outputpath_rawdata, queryYearString, fileName)
%DEFINEJAVA PATH FOR PARSING XML
  javaaddpath('/opt/xerces-2_11_0/xml-apis.jar');
  javaaddpath('/opt/xerces-2_11_0/xercesImpl.jar');

  fprintf('\r\n\r\n***Start of processing: %s\r\n',fileName);

more off

%  queryYearString = '2015/'
queryYearString = [queryYearString '/'];
%  queryYearString = [queryYearString '/'];
%  outputpath_rawdata = '/home/abv/CLIENT_gravitylab/financialdata/';
%fileName = 'affiliates_clickbooth_com-raw_02-Dec-2015-154957_CampaignSummary.xml';

%get filename date string
I1 = strfind(fileName,'raw_');
%I2 = strfind(fileName,'_CampaignSummary);
fileNameDateString = fileName(I1+4:I1+4+17);

I1 = strfind(fileName,'-raw');
fileNamePartnerString = fileName(1:I1-1);

%fileNamePartnerString

%fileNameDateString


inputFileNameRoot = fileName(1:end-4);
outputFileName = [inputFileNameRoot '.csvgl'];

filePathName =[outputpath_rawdata queryYearString fileName];
outputFilePathName = [outputpath_rawdata queryYearString outputFileName];
 

%%%%%%%%%%%%%%%%%%%%%5 
%%LOAD THE INFO FILE FOR THIS PARTNER
I = strfind(fileName,'-apitype');
fileName_info = [outputpath_rawdata queryYearString fileName(1:I-1) '-info.txt'];

fprintf('Loading and parsing info file: %s\r\n',fileName_info);
[info, successflag]= oc_parsefile(fileName_info);

info

 business_segment = info.businessSegment;
 business_segment_child = info.businessSegmentChild;
 transaction_type = info.transactionType
 gravitylab_id = info.GLID;
 partner_name = info.partnerName;
 query_date = info.queryDate;
 
 
 
 
 
 
%filename = 'sample.xml';
 
 % These 3 lines are equivalent to xDoc = xmlread(filename) in matlab
 
global parser
global xDoc

 parser = javaObject('org.apache.xerces.parsers.DOMParser');
 parser.parse(filePathName); 
 xDoc = parser.getDocument;
 


%



 % get first data element 
 %elem = xDoc.getElementsByTagName('row_count').item(0); %first item with tag row_count
 % get text from child
 %dataString = elem.getFirstChild.getTextContent;
 %data = str2num(dataString);
 %numberEntries = data; %indexed in the xml 0 to 75-1

%%%%% READ ALL DATA

cnt = 1
writeString =[];
%for idx = 0:numberEntries-1
idx = 0;
while ~isempty( getinfo('data',idx) )
   %elem = xDoc.getElementsByTagName('offer_id').item(idx); %first item with tag row_count
   %offer_idString = elem.getFirstChild.getTextContent;
   
   offer_id = getinfo('campaignid',idx);
   offer_name = getinfo('name',idx);
   %business_segment = getinfo('description',idx);   
   transaction_amount = getinfo('payout',idx);
   unit = getinfo('unit',idx);
   %daysleft = getinfo('daysleft',idx)
   %bannercount = getinfo('bannercount',idx)
   %emailcount = getinfo('emailcount',idx)
   %textcount = getinfo('textcount',idx)
   vertical_name = getinfo('category',idx);
   geotargeting = getinfo('geotargeting',idx);
   
   
   %pause
   %vertical_name = getinfo('vertical_name',idx);
   %price_format = getinfo('price_format',idx);
   %price = getinfo('price',idx);
   %impressions = getinfo('impressions',idx);
   %clicks = getinfo('clicks',idx);
   %conversions = getinfo('conversions',idx);
   %conversion_rate = getinfo('conversion_rate',idx);
   %currency_symbol = getinfo('currency_symbol',idx);
   %transaction_amount = getinfo('revenue',idx); %for cake this is revenue, however for other is might not be. Refer to transaction type
   %epc = getinfo('epc',idx);

   blank = ' ';
   partner_id = ' '; %This is the affiliate ID that they give us
   %offer_name=' ';
   offer_details=' ';
   click_id =' ';
   ip_address = ' ';
   subid1=' ';
   subid2=' ';
   subid3=' ';
   thisrpm =' ';
   transaction_date = ' ';
   price_format = ' ';
   price = ' ';
   impressions = ' ';
   clicks = ' ';
   conversions = ' ';
   conversion_rate = ' ';
   currency_symbol = ' ';
   epc = ' ';
   
   %business_segment = ' '; %email, social,video, etc
   %business_segment_child = ' '; %subset of email, social, video, etc
   %transaction_type = ' '; % revenue, cost of goods, fees
   %gravitylab_id = ' '; %generated combining the partner_id concatenated with 4 digit internalID EXAMPLE: gravityID_partnerID
   %partner_name = ' '; %get from site API info
   
   
   writeString{cnt} = [query_date ',' transaction_date ',' business_segment ',' business_segment_child ',' transaction_type ',' transaction_amount ',' fileNameDateString ',' ...
         gravitylab_id ',' partner_name ',' fileNamePartnerString ',' partner_id ',' ...
         offer_id ',' offer_name ',' offer_details ',' vertical_name ',' price_format ',' price ',' impressions ',' click_id ',' ...
         clicks ',' conversions ',' ip_address ',' subid1 ',' subid2 ',' subid3 ',' conversion_rate ',' currency_symbol ',' thisrpm ',' epc];

         %remove dollar signs
         writeString{cnt} = strrep(writeString{cnt},'$','');
         
idx = idx +1;
cnt = cnt +1;
end %eof while


fprintf('completed reading XML. Now writing to file.\r\n');
%pause

%size(writeString)
numberEntries_writeString = size(writeString,2);
fprintf('Opening : *%s* for writing\r\n',outputFilePathName);

fid = fopen(outputFilePathName,'w');

%check if file opened correctly
if fid >= 1
  fprintf('file opened successfully for writing\r\n');
else 
  fprintf('Error opening file for writing %s. Aborting.\r\n',outputFilePathName);
  fclose(fid); %surrender handle
  return;
end

%write header: DO NOT WRITE HEADER IF BATCH THIS FUNCTION
%fprintf(fid,'offer_id,vertical_name,price_format,price,impressions,clicks,conversions,conversion_rate,currency_symbol,revenue,epc\n');


for cnt = 1:numberEntries_writeString
  fprintf(fid,'%s \n',char(writeString{cnt})); %write for Windows only use cr
end  
fclose(fid);





%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%%%%%%%%%%%% AUX FUNCTIONS


function [dataString] = getinfo(tagString,idx)


  global parser
  global xDoc

   elem = xDoc.getElementsByTagName(tagString).item(idx); %first item with tag row_count
   if isempty(elem)
     dataString =[];
     return
   end
   dataString = elem.getFirstChild.getTextContent;

   %remove trailing spaces, CR and LF
   %fprintf('parsing\r\n');
   dataString = deblank(dataString);
   
   dataString = strrep(dataString,sprintf('\r'),'');
   dataString = strrep(dataString,sprintf('\n'),'');
   





 % get first data element 
% elem = xDoc.getElementsByTagName('campaign').item(0);
 % get text from child
% data = elem.getFirstChild.getTextContent
 % get attribute named att
% att = elem.getAttribute('att')
