%
%%
%
%
%
% Gets data for the previous day. (Start and End dates = today - 1 day.
%
% Designed to be executed daily at 5am Pacific time by CROND
%
% Dates are currently specified such that the start and end date in a range query
% are the same day.  It needs to be investigated further if any of these APIs require
% date "encapsulization" rather than "inclusive" date queiries
%
%
%


function [resultString] = pull_financialdata_daily()

resultString=[]; %return empty on error



dateFormatOut = 'mm/dd/yyyy';

%get this days date and form to string MM/DD/YYYY
todayDateNum = floor(now); %the number of days between today and Jan1, 0000

%create an "inclusive" start and end date query
yesterdayDateNum = todayDateNum-1;
todayDateNum = yesterdayDateNum; %USE SAME START AND END DATE. GET DATA FROM PREV DAY

%transform date nums to a standard date string
endDateString = datestr(todayDateNum,dateFormatOut);
startDateString = datestr(yesterdayDateNum,dateFormatOut);


% Call function that queries remote web sites and saves the raw query output as XML OR CSV files
% to be further processed in a subsequent step.
[resultString] = pull_financialdata(startDateString,endDateString);



