%
%
%
%
% Read todays date file: summary_mm-dd-yyyy.csv and cat it to the running file.
% This function runs 1 time each day.
%
function [successflag] = combine_dailycsv2total()

successflag = 1;

totalSummaryAllFilePathName = '/home/abv/CLIENT_gravitylab/financialdata/gravitylab/livedata/summary_cumulativedata.csv';

yesterdayDateString = datestr(now-1,'mm-dd-yyyy');
dailySummaryAllFilePathName = ['/home/abv/CLIENT_gravitylab/financialdata/gravitylab/livedata/hourly/' sprintf('summary_%s.csv',yesterdayDateString)  ]; %follow symb link to samba share


%Load the data gathered for this day
st = fileread(dailySummaryAllFilePathName);



%Append the data gathered for this day to the larger file
fid = fopen(totalSummaryAllFilePathName,'a');
fprintf(fid,'\r\n');
fprintf(fid,'%s',st);
fclose(fid);


