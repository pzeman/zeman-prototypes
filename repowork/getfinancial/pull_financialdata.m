%
%
%   Pull financial data from various sources
%
%   
% Support for multiple API types including: cake, etc.
%
%
%
%   startDateString is MM/DD/YYYY
%   endDateString is MM/DD/YYYY for Campaign Summary, or [] for Performance Summary
%
%
% Notes for future:
%   - what do we do if a site is down when we poll for data? Currently, those data will be missing from the summary
%   
%
%
%





function [resultString] = pull_financialdata(startDateString,endDateString,interfaceTypeSearchString)
resultString =[];

%interfaceTypeSearchString = 'cake';



%DEFINE VARIABLES
global errorFilePathName
global outputpath_rawdata

%DEFINE PATH TO FINANCIAL DATA ROOT
outputpath_rawdata = '/home/abv/CLIENT_gravitylab/financialdata/';
errorFilePathName = [outputpath_rawdata sprintf('caughterrors_called_date_%s.log',datestr(now,'mm-dd-yyyy-hh-ss'))  ];

%CHECK FOR MALFORMED DATE STRINGS

if length(startDateString) ~= 10 || length(endDateString) ~= 10
  fprintf('ERROR: One of the date strings is too short\r\n');
return
end


I = strfind(startDateString,'/');
if str2num(startDateString(1:I(1)-1)) > 12
    fprintf('Date Error: Please supply date in format: MM/DD/YYYY\r\n');
    return;
end

if ~isempty(endDateString)
  I = strfind(endDateString,'/');
  if str2num(startDateString(1:I(1)-1)) > 12
    fprintf('Date Error: Please supply date in format: MM/DD/YYYY\r\n');
    return;
  end
end


%MOVE FILES FROM PREVIOUS API PULL TO HISTORICAL OLD QUERIES STORAGE 
sourceMask = [outputpath_rawdata endDateString(7:end) '/*-apitype_' interfaceTypeSearchString '*.*'];
%destinationMask = [outputpath_rawdata endDateString(7:end) '/old_queries/'];
destinationFolder = [outputpath_rawdata endDateString(7:end) '/old_queries/'];
fprintf('Moving files from old queries: %s TO %s\r\n',sourceMask,destinationFolder);
d = dir(sourceMask);
if min(size(d)) == 0
  fprintf('   No files to move\r\n');
else
  fprintf('   Moving files from previous query\r\n');
  %Check that directory exists:
  if exist(destinationFolder) == 0
   %make the folder if it does not exist.
    fprintf('Making folder: %s\r\n',destinationFolder);
    mkdir(destinationFolder);  
  end  
  movefile(sourceMask,destinationFolder,'f');
end





% HAD ERROR at fprintf -- CORRECTED AND WORKING
% q(1).queryTypeString = 'getcampaigns';
% q(1).requestTypeString = 'post';
% q(1).interfaceType = 'transparentads';
% q(1).siteUrlString = 'www.drivotracker.com'; 
% q(1).apiKey = '398c777ea7081b1fa3dc22942b5eb7c3c717c555b431fde582f338c67f9fcb58';
% q(1).affiliateId = '';
% q(1).securtyString = 'https://';
% q(1).partnerName = 'drivotracker.com';
% q(1).outputFormat = 'xml';
% q(1).businessSegment ='email';
% q(1).businessSubsegment = '';
% q(1).callFrequencyMinutes = 0; 
% q(1).businessSegment = 'email';
% q(1).businessSegmentChild = 'default';
% q(1).transactionType = 'default';
% q(1).GLID = 'GL00023';
 
 
 
 
 
 %ERROR? This one ALWAYS RETURNS AN EMPTY STRING
 %q(1).queryTypeString = 'clicks'; %clicks, sales, or test
 %q(1).requestTypeString = 'get';
 %q(1).interfaceType = 'adgenics';
 %q(1).siteUrlString = 'reporting.gwmtracker.com';
 %q(1).apiKey = 'cad59c17469c3cf4ca9d9b6ed19c8c7dae74668cf2ac489b';
 %q(1).affiliateId = '3200064'; %key not used for api call
 %q(1).partnerName = 'gwmtracker.com';
 %q(1).outputFormat = 'csv';
 %q(1).transactionType = 'revenue';
 %q(1).businessSegment ='email';
 %q(1).businessSubsegment = '';
 %q(1).callFrequencyMinutes = 15; 
 %q(1).businessSegment = 'email';
 %q(1).businessSegmentChild = 'default';
 %q(1).transactionType = 'default';
 %q(1).GLID = 'GL00017';
 
 
 
 
%%%%%%%%%
% GET LIST OF SITES OF SPECIFIED TYPE
 q = getPartnerSites(interfaceTypeSearchString);
 
  
%%%%%%%%%%
% ADD SOME TEMPORARY FIELDS TO q to pass into the summary csv files
 for i = 1:size(q,2)
   q(i).queryDateString = [startDateString '-' endDateString];
 
 end
 
%%%%%%%%%
% POLL EACH SITE FOR DATA
 
 N = size(q,2);
 for i = 1:N
    resultString  = getApiResult(q(i),outputpath_rawdata,startDateString,endDateString);
    %if isempty(resultString)
    %end
 end
 
 





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% AUX FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%
	
function [resultString] = getApiResult(q,outputpath_rawdata,startDateString,endDateString)
  
  %DEFINEJAVA PATH FOR PARSING XML
  %javaaddpath('/opt/xerces-2_11_0/xml-apis.jar');
  %javaaddpath('/opt/xerces-2_11_0/xercesImpl.jar');
  
 
 global errorFilePathName
  
queryTypeString = q.queryTypeString;
 
 
 fprintf('\r\n\r\n***** STARTING POLL FOR NEW DATA:\r\n');
 
 
 
 if strcmp(queryTypeString,'CampaignSummary') %cake
 
 %queryTypeString = 'CampaignSummary';
 %startDateString = '12/09/2015'; % '01/01/2014'; MONTH, DAY , YEAR
 %endDateString = '12/10/2015'; % '12/31/2015';
 eventTypeString = 'all';
 startAtRowString = '1';
 rowLimitString = '0';

 elseif strcmp(queryTypeString,'PerformanceSummary') %cake
    %queryTypeString = 'PerformanceSummary';
     %dateString = '11/11/2015'; %month day year?
      dateString = startDateString;
 
 elseif strcmp(queryTypeString,'clicks') %adgenics
 
 
 %elseif strcmp(queryTypeString,'clicks') %adgenics
 
 
  
 
 else
   % fprintf('Error: queryTypeString invalid: %s\r\n',queryTypeString);
    %resultString =[];
   % return
 
 end
 
 
 interfaceType = q.interfaceType;
 siteUrlString = q.siteUrlString;
 apiKey = q.apiKey;
 affiliateId = q.affiliateId;
 
 
 


 
 
 
 
 
 
 

% Get performance summary suggested by our account manager
if (strcmp(queryTypeString,'PerformanceSummary')) && (strcmp(interfaceType,'cake'))
   siteUrlPrefixString = 'http://';  
   getSummaryString = [siteUrlPrefixString siteUrlString '/affiliates/api/2/reports.asmx/' queryTypeString '?api_key=' apiKey '&affiliate_id=' affiliateId '&date=' dateString];

elseif (strcmp(queryTypeString,'CampaignSummary')) && (strcmp(interfaceType,'cake'))
   siteUrlPrefixString = 'http://';
   getSummaryString = [siteUrlPrefixString siteUrlString '/affiliates/api/2/reports.asmx/' queryTypeString '?api_key=' apiKey '&affiliate_id=' affiliateId '&start_date=' startDateString '&end_date=' endDateString '&event_type=' ...
                       eventTypeString '&start_at_row=' startAtRowString '&row_limit=' rowLimitString ];


elseif (strcmp(queryTypeString,'clicks')) && (strcmp(interfaceType,'adgenics'))
   siteUrlPrefixString = 'http://';
   %getSummaryString = [siteUrlPrefixString siteUrlString '/api.php/' queryTypeString '?key=' apiKey '&start=' startDateString '&end=' endDateString '&format=xml&nozip=1'];
   %getSummaryString = [siteUrlPrefixString siteUrlString '/api.php?type=' queryTypeString '&key=' apiKey '&start=' startDateString '&end=' endDateString '&format=xml&nozip=1'];
   getSummaryString = [siteUrlPrefixString siteUrlString '/api.php?type=' queryTypeString '&key=' apiKey '&start=' startDateString '&end=' endDateString '&format=' q.outputFormat '&nozip=1'];
                       
                       
                       
elseif (strcmp(interfaceType,'adnet'))
   siteUrlPrefixString = 'https://';
   %reformat startDateString to YYYY-MM-DD
   tempDateString = [endDateString(7:end) '-' endDateString(1:2) '-' endDateString(4:5)]; 
   getSummaryString = [siteUrlPrefixString siteUrlString '/service/?username=' q.userNameString '&password=' q.passwordString '&format=csv' '&date=' tempDateString ];

elseif strcmp(interfaceType,'interlincx')

   siteUrlPrefixString = 'http://';
   %reformat startDateString to YYYY-MM-DD
   tempEndDateString = [endDateString(7:end) '-' endDateString(1:2) '-' endDateString(4:5)]; 
   tempStartDateString = [startDateString(7:end) '-' startDateString(1:2) '-' startDateString(4:5)]; 
   getSummaryString = [siteUrlPrefixString siteUrlString '/api/account-report?cid=' q.affiliateId '&token=' q.apiKey '&format=csv' '&dateStart=' tempStartDateString '&dateEnd=' tempEndDateString ];


elseif strcmp(interfaceType,'transparentads')

   siteUrlPrefixString = 'https://';
   getSummaryString = [siteUrlPrefixString siteUrlString '/pubapi.php'];

   
elseif strcmp(interfaceType,'ytz')

   siteUrlPrefixString = 'http://';
 %  getSummaryString = [siteUrlPrefixString siteUrlString '/pubapi.php'];
    getSummaryString = [siteUrlPrefixString siteUrlString '/publisher/api/query'];

   
   
                       
                       
else
  %fprintf('****ERROR: Invalid query string provided\r\n');
  %interfaceType
  %queryTypeString
  
    writeErrorLog(q,['Unknown query string interfaceType provided: ' interfaceType]);
   % resultString = '';
    
  return

end


fprintf('Sending getSummarystring: *%s*\r\n',getSummaryString);
fprintf('sending q.requestTypeString: *%s*\r\n',q.requestTypeString);


%resultString = urlread(getSummaryString); %most are get
if strcmp(q.requestTypeString,'post')
      fprintf('Sending POST request\r\n');
      
      if strcmp(interfaceType,'ytz')
        %ytz requires date format changed from MM-DD-YYYY to YYYY-MM-DD
        startDateStringTemp = [startDateString(7:10) '-' startDateString(1:2) '-' startDateString(4:5)];
        endDateStringTemp = [endDateString(7:10) '-' endDateString(1:2) '-' endDateString(4:5)];
        querystring = sprintf("urlread(getSummaryString,q.requestTypeString,{'auth[key]','%s','auth[user]','%s','format','%s','filter[start_date]','%s','filter[end_date]','%s'})",q.apiKey,q.userNameString,q.outputFormat,startDateStringTemp,endDateStringTemp);
      else
      
        
        querystring = sprintf("urlread(getSummaryString,q.requestTypeString,{'apikey','%s','apifunc','%s'})",q.apiKey,q.queryTypeString);
      end
      
      
      %%%% URL READ HERE %%%%%%
      % Catch any errors and report in urlread_errors.log
            
      fprintf('Sending Post: *%s*\r\n',querystring);
      
      try
         resultString = eval(querystring);         
      catch
         writeErrorLog(q,['caught error in eval urlread for POST: ' querystring]);
      end
      
      
      %q.apiKey apifunc=' q.queryTypeString
      % resultstring = urlread(getSummaryString,q.requestTypeString);
else


  %%%% URL READ HERE %%%%%%
  % Catch any errors and report in urlread_errors.log

  fprintf('Sending GET request\r\n');
   
  try
    resultString = urlread(getSummaryString); %most are get
  catch
    writeErrorLog(q,['caught error in urlread for GET: ' getSummaryString]);
    resultString = '';
  end
 

 
  
end


%This should be written as 1 call to read the date because it can cause a year-switch error and puts files in the wrong place.


%IF USING THE DATE THE QUERY IS MADE
thisDateNum = now;
thisDateNumString = sprintf('%i',floor(thisDateNum*100000));
%thisDate = datestr(now);
%yearString = datestr(now,'yyyy');
%dateTimeString = strrep(thisDate,' ','-');
%dateTimeString = strrep(dateTimeString,':','');

%IF USING THE QUERY DATE TO POLL: startDateString, endDateString
yearString = startDateString(7:10);
dateTimeString = startDateString;
dateTimeString = strrep(dateTimeString,'/','-');

siteUrlStringFix = strrep(siteUrlString,'.','_');



%write to raw data file with date time coding in filenam
rawFileName = [siteUrlStringFix '-apitype_' q.interfaceType  '-raw_' dateTimeString '_' thisDateNumString '_' queryTypeString '.' q.outputFormat];
filepath = [outputpath_rawdata yearString '/'];
filepathname = [filepath rawFileName];
filepathname_partnerinfo = [filepath siteUrlStringFix '-info.txt'];






%CHECK IF EMPTY BEFORE OTHER CHECKS
fprintf('Checking for errors before writing...\r\n');
if isempty(resultString)

  if exist('getSummaryString')
    writeErrorLog(q,sprintf('CALLED: %s',getSummaryString));
  end



  writeErrorLog(q,'empty string returned');
  fprintf('resultString is:\r\n');
  resultString
  resultString =''; 

%CHECK IF HTML was was returned instead of XML or CSV format
elseif strcmp(upper(resultString(1:6)), '<HTML>')
  %fprintf('Error: resultString contains HTML, not XML OR CSV FORMAT\r\n');
  if exist('getSummaryString')
    writeErrorLog(q,sprintf('CALLED: %s',getSummaryString));
  end
  writeErrorLog(q,sprintf('resultString contains HTML, not XML or CSV format: %s',resultString )   );
  fprintf('resultString is:\r\n');
  resultString
  resultString = '';
end
  
  

  
mkdir(filepath);
fprintf('Preparing to Write to: %s\r\n',filepathname);

  fprintf('The result string is:\r\n');
  resultString
  
  
%WRITE THE QUERY DATA FILE
fid = fopen(filepathname,'w');
%fprintf(fid,resultString);
fprintf(fid,'%s',resultString); %use this to address bug?

fclose(fid);
fprintf('      Writing complete.\r\n');



%WRITE PARTNER DATA TO FILE TO BE READ WHEN CREATING CSV SUMMARY
partnerInfoString = ['businessSegment=' q.businessSegment ';businessSegmentChild=' q.businessSegmentChild ';transactionType=' q.transactionType ';GLID=' q.GLID ';partnerName=' q.partnerName ';queryDate=' q.queryDateString ';'];

fprintf('Preparing to write to: %s\r\n',filepathname_partnerinfo);
fid = fopen(filepathname_partnerinfo,'w');
fprintf(fid,partnerInfoString);
fclose(fid);
fprintf('      Writing complete.\r\n');
