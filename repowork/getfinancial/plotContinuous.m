%
%
%
% Create line plot of all samples in time
%


function [] = plotContinuous(transferTypeString)

%transferTypeString = 'adgenics';
queryDateString = '01-04-2016';

%dataPath = '/media/NAS_clinezeman/clinezeman/CLIENT_gravitylab/financialdata/';
dataPath ='/home/abv/CLIENT_gravitylab/financialdata/gravitylab/livedata/hourly/'; %follow symb link to samba share

FileMask = ['summary_*-apitype_' sprintf(transferTypeString) '*.csv'];



% Read the directory get the existing files to load

dataPathFileMask = [dataPath FileMask];
fprintf('Sanning: %s\r\n',dataPathFileMask);


d = dir(dataPathFileMask);
%files are in order by date based on file name

numberFiles = size(d,1);



%GET ALL DATA IN DIRECTORY FOR THIS TYPE
for i = 1:numberFiles
%get the data
tempfilestring = [dataPath d(i).name];
fprintf('Loading: %s\r\n',tempfilestring);
 [successFlag, pStruct(i)] = plotDailySummary(tempfilestring,queryDateString,0);
end


%DETERMINE NUMBER OF CATEGORES
numberCategoriesList = [];
for i = 1:size(pStruct,2);
 numberCategoriesList = [numberCategoriesList pStruct(i).numberCategories];
end
numberCategoriesMax = max(numberCategoriesList);


%%%%PLOT SUM REVENUE

rvListSum=[];
for i = 1:size(pStruct,2)
 rvList =[];
 for k = 1:pStruct(i).numberCategories
   rvList = [rvList pStruct(i).categoryIdx(k).revenue];
 end
 rvListSum(i) = sum(rvList);
end
   

figure; plot(rvListSum);
title('Sum of all categories revenue');
ylabel('Revenue $');
xlabel('Sample (every hour)');






