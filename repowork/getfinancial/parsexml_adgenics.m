%
%
%
% Parse the adgenics styled  xml financla data
%
%
% Case sensitive date string: example: 02-Dec-2015
% query year string: '2015'
%

function [] = parsexml_adgenics(queryDateString,queryYearString)

%queryYearString = '2015';
%queryDateString = '02-Dec-2015';
queryTypeString = 'clicks';

outputpath_rawdata = '/home/abv/CLIENT_gravitylab/financialdata/';



PartnersList = {'reporting_gwmtracker_com'};



numberPartners = size(PartnersList,2);



for i =1:numberPartners

  %   fileName = [outputpath_rawdata queryYearString '/' cakePartnersList{i} '-raw_' queryDateString queryTypeString '.xml'];
  fileNameMask = [PartnersList{i} '-raw_' queryDateString '*' '_' queryTypeString '.xml'];
 
  d = dir([outputpath_rawdata queryYearString '/' fileNameMask]);
  size(d)
  if size(d,1) == 0
    fprintf('mask found no files %s\r\n',fileNameMask);
    break; 
    %pause
  end

  if size(d,1) > 1
    fprintf('found multiple files with same mask: %s\r\n',fileNameMask);
    break;
   end

  fileName = d(1).name;

  parsexml_aux(outputpath_rawdata, queryYearString, fileName)



end






%%%%%%%%%%%%%%%%AUX FUNCTIONS %%%%%%%%%%%%%%%%%%




function [] = parsexml_aux(outputpath_rawdata, queryYearString, fileName)
%DEFINEJAVA PATH FOR PARSING XML
  javaaddpath('/opt/xerces-2_11_0/xml-apis.jar');
  javaaddpath('/opt/xerces-2_11_0/xercesImpl.jar');


more off

%  queryYearString = '2015/'
queryYearString = [queryYearString '/'];
%  queryYearString = [queryYearString '/'];
%  outputpath_rawdata = '/home/abv/CLIENT_gravitylab/financialdata/';
%fileName = 'affiliates_clickbooth_com-raw_02-Dec-2015-154957_CampaignSummary.xml';

%get filename date string
I1 = strfind(fileName,'raw_');
%I2 = strfind(fileName,'_CampaignSummary);
fileNameDateString = fileName(I1+4:I1+4+17);

I1 = strfind(fileName,'-raw');
fileNamePartnerString = fileName(1:I1-1);

%fileNamePartnerString

%fileNameDateString


inputFileNameRoot = fileName(1:end-4);
outputFileName = [inputFileNameRoot '.csv'];

filePathName =[outputpath_rawdata queryYearString fileName];
outputFilePathName = [outputpath_rawdata queryYearString outputFileName];
 

%filename = 'sample.xml';
 
 % These 3 lines are equivalent to xDoc = xmlread(filename) in matlab
 
global parser
global xDoc

 parser = javaObject('org.apache.xerces.parsers.DOMParser');
 parser.parse(filePathName); 
 xDoc = parser.getDocument;
 


%FIRST GET ROW COUNT SO WE KNOW HOW MANY ENTRIES



 % get first data element 
 elem = xDoc.getElementsByTagName('row_count').item(0); %first item with tag row_count
 % get text from child
 dataString = elem.getFirstChild.getTextContent;
 data = str2num(dataString);
 numberEntries = data; %indexed in the xml 0 to 75-1

%%%%% READ ALL DATA

cnt = 1
writeString =[];
for idx = 0:numberEntries-1

   %elem = xDoc.getElementsByTagName('offer_id').item(idx); %first item with tag row_count
   %offer_idString = elem.getFirstChild.getTextContent;
   
   offer_id = getinfo('offer_id',idx);
   vertical_name = getinfo('vertical_name',idx);
   price_format = getinfo('price_format',idx);
   price = getinfo('price',idx);
   impressions = getinfo('impressions',idx);
   clicks = getinfo('clicks',idx);
   conversions = getinfo('conversions',idx);
   conversion_rate = getinfo('conversion_rate',idx);
   currency_symbol = getinfo('currency_symbol',idx);
   revenue = getinfo('revenue',idx);
   epc = getinfo('epc',idx);

   %blank = ' ';
   %partner_id = ' ';
   %offer_name=' ';
   %offer_details=' ';
   %click_id =' ';
   %ip_address = ' ';
   %subid1=' ';
   %subid2=' ';
   %subid3=' ';
   %thisrpm =' ';

   %writeString{cnt} = [blank ',' blank ',' blank ',' revenue ',' fileNameDateString ',' fileNamePartnerString ',' partner_id ',' offer_id ',' offer_name ',' offer_details ',' vertical_name ',' price_format ',' price ',' impressions ',' click_id ',' clicks ',' conversions ',' ip_address ',' subid1 ',' subid2 ',' subid3 ',' conversion_rate ',' currency_symbol ',' thisrpm ',' epc];

   writeString{cnt} = ' ';

cnt = cnt +1;
end %eof for


fprintf('completed reading XML. Now writing to file.\r\n');


%size(writeString)
numberEntries_writeString = size(writeString,2);
fprintf('Opening : *%s* for writing\r\n',outputFilePathName);

fid = fopen(outputFilePathName,'w');

%check if file opened correctly
if fid >= 1
  fprintf('file opened successfully for writing\r\n');
else 
  fprintf('Error opening file for writing %s. Aborting.\r\n',outputFilePathName);
  fclose(fid); %surrender handle
  return;
end

%write header: DO NOT WRITE HEADER IF BATCH THIS FUNCTION
%fprintf(fid,'offer_id,vertical_name,price_format,price,impressions,clicks,conversions,conversion_rate,currency_symbol,revenue,epc\n');


for cnt = 1:numberEntries_writeString
  fprintf(fid,'%s \n',char(writeString{cnt})); %write for Windows only use cr
end  
fclose(fid);





%%%%%%%%%%%%%%%%%%%%%%%%5
%%%%%%%%%%%%%%% AUX FUNCTIONS


function [dataString] = getinfo(tagString,idx)


  global parser
  global xDoc

   elem = xDoc.getElementsByTagName(tagString).item(idx); %first item with tag row_count
   dataString = elem.getFirstChild.getTextContent;






 % get first data element 
% elem = xDoc.getElementsByTagName('campaign').item(0);
 % get text from child
% data = elem.getFirstChild.getTextContent
 % get attribute named att
% att = elem.getAttribute('att')
