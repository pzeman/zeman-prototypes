%
%
% Example:
% 
%
% 

function [successFlag, plotStruct] = plotDailySummary(filePathName,startDateString,PLOTFLAG)
successFlag = 1;


startDateString = strrep(startDateString,'/','-');

%csvdat = csvread(filePathName);

if ~exist('startDatestring')
   startDatestring = '';

end


%vertical_name_idx = 9; %15
transaction_amount_idx =6;
clicks_idx = 20;

fprintf('LOADING: %s\r\n',filePathName);


 lineArray = readmixedcsvall(filePathName,',');
 
 numberColumns = size(lineArray,2);
 numberEntries = size(lineArray,1);
 
 
 %Determine which column contains the offer names (could be 13 14 or 15), pick the shortest non-blank one
 idxpossible = [12 13 14 15];
 temp1 = strrep( char(lineArray(1, idxpossible(1) )),' ','');
 temp2 = strrep( char(lineArray(1, idxpossible(2) )),' ','');
 temp3 = strrep( char(lineArray(1, idxpossible(3) )),' ','');
 lens = [length(temp1) length(temp2) length(temp3)];
 [Y I] = max(lens);
 vertical_name_idx = idxpossible(I);
 
 %lineArray{1,vertical_name_idx}
 
 
 %remove whitespaces and shorten
 for i = 1:numberEntries
   temp = char(lineArray{i,vertical_name_idx});
   temp = strrep(temp,' ','');
   temp = strrep(temp,'_',''); 
   
   if length(temp) > 21
   %fprintf('shorening\r\n');
   temp = temp(end-20:end);
   end
   lineArray{i,vertical_name_idx} = temp;
 end
 
 
% lineArray{1,vertical_name_idx}
 
 
 %pause
 %Auto generate categories List
 categoryRaw ={};
 for i = 1:numberEntries
 temp = upper(char(lineArray{i,vertical_name_idx}));
 
 %temp
 
 %pause
 
 %remove ALL white spaces
 temp = strtrim(temp);
 temp = strrep(temp,' ','');
 categoryRaw{i} = temp;
 end
 categoryList = getUniqueCA(categoryRaw);
 numberCategories = size(categoryList,2);
 
 
 for i = 1:numberCategories
 %upper(char(categoryList(i))) 
 %pause
 
   categoryIdx(i).idxList = strmatch( upper(char(categoryList(i)))    , upper(char(lineArray{:,vertical_name_idx}))   ); 
 end
 
 %Get the revenue, clicks for each category
 for i = 1:numberCategories
    I = categoryIdx(i).idxList;
    categoryIdx(i).revenue = sum(str2num(char(lineArray{I,transaction_amount_idx})));
    categoryIdx(i).clicks = sum(str2num(char(lineArray{I,clicks_idx})));
 end
 
 %Get the revenue for each category
 for i = 1:numberCategories
    I = categoryIdx(i).idxList;
    categoryIdx(i).revenue = sum(str2num(char(lineArray{I,transaction_amount_idx})));
 end
 
 
 
 %MAKE PLOTS
 plotStruct.numberCategories = numberCategories;
 plotStruct.categoryIdx = categoryIdx;
 plotStruct.categoryList = categoryList;
 plotStruct.startDateString = startDateString;
 

if PLOTFLAG == 1
 doplots(numberCategories, categoryIdx,categoryList,startDateString);
 end







 
 %%%%%%%%%%%%%%%% AUX FUNCTIONS %%%%%%%%%%%%%%
 
 function [] = doplots(numberCategories,categoryIdx,categoryList,startDateString)
 
 %Make plot of revenue by category
 rvList = [];
 for i = 1:numberCategories
   rvList = [rvList categoryIdx(i).revenue];
 end
 figure;
 bar((rvList));
 ax = gca;
 set(ax,'XTickLabel',categoryList);
 h = get(gca,'xlabel');
 xlabelstring = get(h,'string');
 xlabelposition = get(h,'position');
 yposition = xlabelposition(2);
 yposition = repmat(yposition,size(categoryList,2),1);
 set(gca,'xtick',[]);
 xtick = 1:size(categoryList,2);
 hnew = text(xtick,yposition,categoryList);
 set(hnew,'rotation',90,'horizontalalignment','right');
 ylabel('$ Revenue');
 title(sprintf('%s',startDateString));
 
 
 
 
 %Make plot of CLICKS by category
 rvList = [];
 for i = 1:numberCategories
   rvList = [rvList categoryIdx(i).clicks];
 end
 figure;
 bar((rvList));
 ax = gca;
 set(ax,'XTickLabel',categoryList);
 h = get(gca,'xlabel');
 xlabelstring = get(h,'string');
 xlabelposition = get(h,'position');
 yposition = xlabelposition(2);
 yposition = repmat(yposition,size(categoryList,2),1);
 set(gca,'xtick',[]);
 xtick = 1:size(categoryList,2);
 hnew = text(xtick,yposition,categoryList);
 set(hnew,'rotation',90,'horizontalalignment','right');
 ylabel('Clicks');
 title(sprintf('%s',startDateString));
 
 
 
 
 
 
