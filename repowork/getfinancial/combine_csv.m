%
%
% Generate preport by combining files.
% Combine the CSV files for a specific date specified
%
%
function [successFlag,outputFilePathName] = combine_csv(startDateString,interfaceTypeSearchString,dateNum)
successFlag = 1;

%dateNum = ceil(now*100000);


startDateString = strrep(startDateString,'/','-');

%Search the stored files for the following info.
%queryDateString = '11-Dec-2015';
queryDateString = startDateString;
%queryYearString = '2015';
queryYearString = startDateString(7:10);


financialDataDirectory = '/home/abv/CLIENT_gravitylab/financialdata/';
financialDataSummaryDirectory ='/home/abv/CLIENT_gravitylab/financialdata/gravitylab/livedata/hourly/'; %follow symb link to samba share

%financialDataDirectory = '/home/abv/CLIENT_gravitylab/gravitylab/livedata/hourly/'; %follows symb ln to samba share


outputFilePathName = [financialDataSummaryDirectory sprintf('summary_%s-apitype_%s_%i.csv',queryDateString,interfaceTypeSearchString,dateNum)];


searchFilePathNameMask = [financialDataDirectory queryYearString '/' sprintf('*-apitype_%s*%s*.csvgl',interfaceTypeSearchString,queryDateString)];
fprintf('Searching using mask: %s\r\n',searchFilePathNameMask);


d = dir(searchFilePathNameMask);

numberFiles = size(d,1);
fprintf('Found %i files to process\r\n', numberFiles);


masterStructString=[];
cnt = 1;
for i = 1:numberFiles
  temp = [financialDataDirectory queryYearString '/' d(i).name];



fid = fopen(temp,'r'); %only read only
tline = fgets(fid);
while ischar(tline)
disp(tline)
  masterStructString{cnt} = tline;
cnt = cnt+1;
tline = fgets(fid);
end
fclose(fid);

 
%  masterStructString{i} = 
end


%%%%%%
fprintf('Writing data to combination file : %s\r\n',outputFilePathName);

fid = fopen(outputFilePathName,'w');
Nentries = size(masterStructString,2);
for i = 1:Nentries
  fprintf(fid,'%s',char(masterStructString{i}));


end



fclose(fid);






